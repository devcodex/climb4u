-- Generate Roles
INSERT INTO public.role (id, name) VALUES (1, 'ROLE_USER');
INSERT INTO public.role (id, name) VALUES (2,'ROLE_OFFICIAL');
INSERT INTO public.role (id, name) VALUES (3,'ROLE_ADMIN');

-- Generate users
-- Admin account
INSERT INTO public.user_account (id, creation_date, email, enabled, last_login, password, profile_image, username) VALUES (0, '2019-06-17 11:19:13.904', 'admin@climb4u.com', true, NULL, '$2a$12$L5cBX.c4w/5LlHycuI.OBOYLJl3kzayI1PP3KqGZUPq2yh//cW2rG', 'https://upload.wikimedia.org/wikipedia/commons/5/50/RedPandaFullBody.JPG', 'admin');
-- Official account
INSERT INTO public.user_account (id, creation_date, email, enabled, last_login, password, profile_image, username) VALUES (1, '2019-06-17 11:19:14.341', 'official@climb4u.com', true, NULL, '$2a$12$eo/BjH4N3KbX9UREwsv6K.czGUPS0XNwphFPH1ebqmu.FrOMGi86O', NULL, 'official');
-- User accounts
INSERT INTO public.user_account (id, creation_date, email, enabled, last_login, password, profile_image, username) VALUES (2, '2019-06-17 11:19:14.341', 'user1@climb4u.com', true, NULL, '$2a$12$UYIyBH0tvc9sujYgOwzrNe5sJkX/.D1ShdYoDC/kHu3yi9QyPXxbK', NULL, 'user1');
INSERT INTO public.user_account (id, creation_date, email, enabled, last_login, password, profile_image, username) VALUES (3, '2019-06-17 11:19:14.341', 'user2@climb4u.com', true, NULL, '$2a$12$YMAHGd7BLpAt8qYfC3L.9OOiZSrr.iBVc9bMR18nT6A1byN1NzAOO', NULL, 'user2');
INSERT INTO public.user_account (id, creation_date, email, enabled, last_login, password, profile_image, username) VALUES (4, '2019-06-17 11:19:14.341', 'user3@climb4u.com', true, NULL, '$2a$12$.D1uchXgedCv0MvgFOoaNOOQqaQa808W8vjrsRLlZa/X7/Bh/nIne', NULL, 'user3');

-- Generate user rights
-- Roles for admin account
INSERT INTO public.users_roles (user_id, role_id) VALUES (0, 1);
INSERT INTO public.users_roles (user_id, role_id) VALUES (0, 2);
INSERT INTO public.users_roles (user_id, role_id) VALUES (0, 3);
--Roles for official account
INSERT INTO public.users_roles (user_id, role_id) VALUES (1, 1);
INSERT INTO public.users_roles (user_id, role_id) VALUES (1, 2);
--Roles for user accounts
INSERT INTO public.users_roles (user_id, role_id) VALUES (2, 1);
INSERT INTO public.users_roles (user_id, role_id) VALUES (3, 1);
INSERT INTO public.users_roles (user_id, role_id) VALUES (4, 1);

-- Generate Departments
insert into department (id, num_department, name) values (0, '1', 'Ain');
insert into department (id, num_department, name) values (1, '2', 'Aisne');
insert into department (id, num_department, name) values (2, '3', 'Allier');
insert into department (id, num_department, name) values (3, '4', 'Alpes-de-Haute-Provence');
insert into department (id, num_department, name) values (4, '5', 'Hautes-Alpes');
insert into department (id, num_department, name) values (5, '6', 'Alpes Maritimes');
insert into department (id, num_department, name) values (6, '7', 'Ardèche');
insert into department (id, num_department, name) values (7, '8', 'Ardennes');
insert into department (id, num_department, name) values (8, '9', 'Ariège');
insert into department (id, num_department, name) values (9, '10', 'Aube');
insert into department (id, num_department, name) values (10, '11', 'Aude');
insert into department (id, num_department, name) values (11, '12', 'Aveyron');
insert into department (id, num_department, name) values (12, '13', 'Bouches-du-Rhône');
insert into department (id, num_department, name) values (13, '14', 'Calvados');
insert into department (id, num_department, name) values (14, '15', 'Cantal');
insert into department (id, num_department, name) values (15, '16', 'Charente');
insert into department (id, num_department, name) values (16, '17', 'Charente-Maritime');
insert into department (id, num_department, name) values (17, '18', 'Cher');
insert into department (id, num_department, name) values (18, '19', 'Corrèze');
insert into department (id, num_department, name) values (19, '2A', 'Corse-du-Sud');
insert into department (id, num_department, name) values (20, '2B', 'Haute Corse');
insert into department (id, num_department, name) values (21, '21', 'Côte-d’Or');
insert into department (id, num_department, name) values (22, '22', 'Côtes d’Armor');
insert into department (id, num_department, name) values (23, '23', 'Creuse');
insert into department (id, num_department, name) values (24, '24', 'Dordogne');
insert into department (id, num_department, name) values (25, '25', 'Doubs');
insert into department (id, num_department, name) values (26, '26', 'Drôme');
insert into department (id, num_department, name) values (27, '27', 'Eure');
insert into department (id, num_department, name) values (28, '28', 'Eure-et-Loir');
insert into department (id, num_department, name) values (29, '29', 'Finistère');
insert into department (id, num_department, name) values (30, '30', 'Gard');
insert into department (id, num_department, name) values (31, '31', 'Haute Garonne');
insert into department (id, num_department, name) values (32, '32', 'Gers');
insert into department (id, num_department, name) values (33, '33', 'Gironde');
insert into department (id, num_department, name) values (34, '34', 'Hérault');
insert into department (id, num_department, name) values (35, '35', 'Ille-et-Vilaine');
insert into department (id, num_department, name) values (36, '36', 'Indre');
insert into department (id, num_department, name) values (37, '37', 'Indre-et-Loire');
insert into department (id, num_department, name) values (38, '38', 'Isère');
insert into department (id, num_department, name) values (39, '39', 'Jura');
insert into department (id, num_department, name) values (40, '40', 'Landes');
insert into department (id, num_department, name) values (41, '41', 'Loir-et-Cher');
insert into department (id, num_department, name) values (42, '42', 'Loire');
insert into department (id, num_department, name) values (43, '43', 'Haute Loire');
insert into department (id, num_department, name) values (44, '44', 'Loire Atlantique');
insert into department (id, num_department, name) values (45, '45', 'Loiret');
insert into department (id, num_department, name) values (46, '46', 'Lot');
insert into department (id, num_department, name) values (47, '47', 'Lot-et-Garonne');
insert into department (id, num_department, name) values (48, '48', 'Lozère');
insert into department (id, num_department, name) values (49, '49', 'Maine-et-Loire');
insert into department (id, num_department, name) values (50, '50', 'Manche');
insert into department (id, num_department, name) values (51, '51', 'Marne');
insert into department (id, num_department, name) values (52, '52', 'Haute Marne');
insert into department (id, num_department, name) values (53, '53', 'Mayenne');
insert into department (id, num_department, name) values (54, '54', 'Meurthe-et-Moselle');
insert into department (id, num_department, name) values (55, '55', 'Meuse');
insert into department (id, num_department, name) values (56, '56', 'Morbihan');
insert into department (id, num_department, name) values (57, '57', 'Moselle');
insert into department (id, num_department, name) values (58, '58', 'Nièvre');
insert into department (id, num_department, name) values (59, '59', 'Nord');
insert into department (id, num_department, name) values (60, '60', 'Oise');
insert into department (id, num_department, name) values (61, '61', 'Orne');
insert into department (id, num_department, name) values (62, '62', 'Pas-de-Calais');
insert into department (id, num_department, name) values (63, '63', 'Puy-de-Dôme');
insert into department (id, num_department, name) values (64, '64', 'Pyrénées Atlantiques');
insert into department (id, num_department, name) values (65, '65', 'Hautes Pyrénées');
insert into department (id, num_department, name) values (66, '66', 'Pyrénées Orientales');
insert into department (id, num_department, name) values (67, '67', 'Bas-Rhin');
insert into department (id, num_department, name) values (68, '68', 'Haut-Rhin');
insert into department (id, num_department, name) values (69, '69', 'Rhône');
insert into department (id, num_department, name) values (70, '70', 'Haute Saône');
insert into department (id, num_department, name) values (71, '71', 'Saône-et-Loire');
insert into department (id, num_department, name) values (72, '72', 'Sarthe');
insert into department (id, num_department, name) values (73, '73', 'Savoie');
insert into department (id, num_department, name) values (74, '74', 'Haute Savoie');
insert into department (id, num_department, name) values (75, '75', 'Paris');
insert into department (id, num_department, name) values (76, '76', 'Seine Maritime');
insert into department (id, num_department, name) values (77, '77', 'Seine-et-Marne');
insert into department (id, num_department, name) values (78, '78', 'Yvelines');
insert into department (id, num_department, name) values (79, '79', 'Deux-Sèvres');
insert into department (id, num_department, name) values (80, '80', 'Somme');
insert into department (id, num_department, name) values (81, '81', 'Tarn');
insert into department (id, num_department, name) values (82, '82', 'Tarn-et-Garonne');
insert into department (id, num_department, name) values (83, '83', 'Var');
insert into department (id, num_department, name) values (84, '84', 'Vaucluse');
insert into department (id, num_department, name) values (85, '85', 'Vendée');
insert into department (id, num_department, name) values (86, '86', 'Vienne');
insert into department (id, num_department, name) values (87, '87', 'Haute Vienne');
insert into department (id, num_department, name) values (88, '88', 'Vosges');
insert into department (id, num_department, name) values (89, '89', 'Yonne');
insert into department (id, num_department, name) values (90, '90', 'Territoire de Belfort');
insert into department (id, num_department, name) values (91, '91', 'Essonne');
insert into department (id, num_department, name) values (92, '92', 'Hauts-de-Seine');
insert into department (id, num_department, name) values (93, '93', 'Seine-St-Denis');
insert into department (id, num_department, name) values (94, '94', 'Val-de-Marne');
insert into department (id, num_department, name) values (95, '95', 'Val-D’Oise');

-- Generate Sites
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (0, 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', true, 'https://picsum.photos/id/198/200/300', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 140, 'https://yahoo.co.jp/consectetuer/adipiscing/elit/proin/risus/praesent/lectus.png', 'Sagittarius serpentarius', true, 'Bloc', 1, 84);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (1, 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', true, 'https://picsum.photos/id/479/200/300', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 156, 'http://mit.edu/augue/vestibulum/rutrum/rutrum/neque/aenean.html', 'Cereopsis novaehollandiae', true, 'Bloc', 3, 42);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (2, 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', true, 'https://picsum.photos/id/479/200/300', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 52, 'https://hibu.com/id/luctus/nec/molestie/sed.jpg', 'Numida meleagris', false, 'Bloc', 3, 28);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (3, 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', true, 'https://picsum.photos/id/744/200/300', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 108, 'http://accuweather.com/ultrices/aliquet/maecenas/leo.jsp', 'Macropus fuliginosus', false, 'Bloc', 4, 15);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (4, 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', true, 'https://picsum.photos/id/479/200/300', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 181, 'https://netvibes.com/nisi/eu/orci/mauris.html', 'Ninox superciliaris', true, 'Bloc', 1, 60);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (5, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', true, 'https://picsum.photos/id/24/200/300', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', 194, 'https://sina.com.cn/sit/amet/sapien/dignissim/vestibulum/vestibulum.jpg', 'Lycosa godeffroyi', false, 'Falaise', 3, 44);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (6, 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', true, 'https://picsum.photos/id/479/200/300', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 102, 'http://wordpress.com/donec/semper/sapien/a/libero/nam.html', 'Macropus eugenii', true, 'Bloc', 1, 70);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (7, 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', true, 'https://picsum.photos/id/296/200/300', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 75, 'https://cbslocal.com/hac.js', 'Camelus dromedarius', true, 'Falaise', 2, 18);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (8, 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', true, 'https://picsum.photos/id/296/200/300', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 195, 'http://facebook.com/quis/justo/maecenas/rhoncus/aliquam.xml', 'Merops bullockoides', true, 'Falaise', 1, 87);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (9, 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', true, 'https://picsum.photos/id/331/200/300', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 145, 'https://ucla.edu/in/faucibus/orci/luctus/et/ultrices.xml', 'Threskionis aethiopicus', false, 'Falaise', 4, 56);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (10, 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', true, 'https://picsum.photos/id/928/200/300', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 136, 'http://ftc.gov/consequat.png', 'Uraeginthus angolensis', true, 'Bloc', 2, 48);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (11, 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', true, 'https://picsum.photos/id/331/200/300', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', 177, 'https://constantcontact.com/amet/nunc/viverra/dapibus/nulla/suscipit/ligula.xml', 'Anastomus oscitans', false, 'Bloc', 2, 45);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (12, 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', true, 'https://picsum.photos/id/296/200/300', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 129, 'https://bbb.org/dis/parturient/montes.jsp', 'Chionis alba', false, 'Bloc', 2, 27);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (13, 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', true, 'https://picsum.photos/id/296/200/300', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.', 128, 'http://squarespace.com/praesent/lectus/vestibulum/quam/sapien/varius.aspx', 'Odocoilenaus virginianus', true, 'Bloc', 3, 35);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (14, 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', true, 'https://picsum.photos/id/928/200/300', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 262, 'https://amazon.com/turpis.png', 'Tachybaptus ruficollis', true, 'Falaise', 2, 92);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (15, 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', true, 'https://picsum.photos/id/744/200/300', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', 188, 'https://scribd.com/nonummy/maecenas/tincidunt/lacus/at/velit/vivamus.jpg', 'Kobus leche robertsi', true, 'Falaise', 1, 50);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (16, 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', true, 'https://picsum.photos/id/331/200/300', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 211, 'http://sakura.ne.jp/tincidunt/nulla/mollis/molestie/lorem/quisque/ut.aspx', 'Ceryle rudis', true, 'Bloc', 0, 46);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (17, 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', true, 'https://picsum.photos/id/743/200/300', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 57, 'http://paginegialle.it/luctus/nec.xml', 'Macropus parryi', false, 'Falaise', 4, 67);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (18, 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', true, 'https://picsum.photos/id/198/200/300', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', 174, 'https://wix.com/id/ligula.png', 'Bettongia penicillata', false, 'Bloc', 2, 71);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (19, 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', true, 'https://picsum.photos/id/131/200/300', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 266, 'http://xinhuanet.com/vivamus.jsp', 'Diomedea irrorata', false, 'Bloc', 3, 41);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (20, 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', true, 'https://picsum.photos/id/743/200/300', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', 196, 'http://devhub.com/felis/ut/at/dolor.jpg', 'Leprocaulinus vipera', false, 'Bloc', 4, 65);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (21, 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', true, 'https://picsum.photos/id/131/200/300', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 282, 'https://dailymail.co.uk/in/porttitor/pede/justo/eu.jsp', 'Dendrocitta vagabunda', true, 'Falaise', 4, 36);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (22, 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', true, 'https://picsum.photos/id/331/200/300', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 165, 'https://ning.com/nulla.json', 'Turtur chalcospilos', false, 'Bloc', 1, 2);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (23, 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', true, 'https://picsum.photos/id/296/200/300', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 296, 'http://mac.com/condimentum/neque/sapien/placerat.xml', 'Acrantophis madagascariensis', false, 'Bloc', 4, 19);
insert into site (id, access_notes, available, cover_image, description, height, location, name, official, type, created_id, department_id) values (24, 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', true, 'https://picsum.photos/id/131/200/300', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', 168, 'https://google.co.uk/nibh/in/lectus/pellentesque/at.js', 'Phascogale calura', false, 'Bloc', 2, 91);

-- Generate Topos
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (0, true, 'https://picsum.photos/id/139/200/300', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 3, 'b', 9, 'c', 199, 40, 14, 'Leprocaulinus vipera', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (1, true, 'https://picsum.photos/id/805/200/300', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 3, 'a', 8, 'c', 174, 3, 8, 'Diomedea irrorata', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (2, true, 'https://picsum.photos/id/238/200/300', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', 6, 'c', 8, 'c', 362, 17, 16, 'Corvus albicollis', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (3, true, 'https://picsum.photos/id/370/200/300', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', 5, 'c', 6, 'b', 411, 37, 8, 'Columba palumbus', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (4, true, 'https://picsum.photos/id/189/200/300', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 5, 'b', 8, 'c', 424, 6, 11, 'Hymenolaimus malacorhynchus', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (5, true, 'https://picsum.photos/id/185/200/300', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 4, 'a', 7, 'a', 495, 20, 3, 'Vulpes cinereoargenteus', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (6, true, 'https://picsum.photos/id/794/200/300', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 5, 'c', 6, 'a', 317, 46, 24, 'Isoodon obesulus', 4, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (7, true, 'https://picsum.photos/id/123/200/300', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 6, 'a', 6, 'b', 147, 9, 16, 'Lasiodora parahybana', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (8, true, 'https://picsum.photos/id/891/200/300', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 3, 'c', 6, 'c', 400, 47, 7, 'Tockus erythrorhyncus', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (9, true, 'https://picsum.photos/id/555/200/300', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 6, 'a', 6, 'a', 449, 15, 24, 'Phoca vitulina', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (10, true, 'https://picsum.photos/id/415/200/300', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', 3, 'a', 6, 'a', 395, 2, 8, 'Corvus albus', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (11, true, 'https://picsum.photos/id/432/200/300', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 5, 'a', 8, 'a', 281, 14, 22, 'Tockus erythrorhyncus', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (12, true, 'https://picsum.photos/id/904/200/300', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 3, 'c', 9, 'b', 474, 32, 19, 'Phalacrocorax varius', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (13, true, 'https://picsum.photos/id/962/200/300', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 5, 'b', 8, 'a', 112, 26, 6, 'Vulpes chama', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (14, true, 'https://picsum.photos/id/378/200/300', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 6, 'a', 6, 'a', 436, 22, 12, 'Zenaida galapagoensis', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (15, true, 'https://picsum.photos/id/447/200/300', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 5, 'b', 9, 'b', 208, 30, 16, 'Dasyurus viverrinus', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (16, true, 'https://picsum.photos/id/665/200/300', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 6, 'b', 6, 'b', 327, 12, 21, 'Macropus eugenii', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (17, true, 'https://picsum.photos/id/122/200/300', 'In congue. Etiam justo. Etiam pretium iaculis justo.', 3, 'c', 9, 'a', 122, 7, 24, 'Terrapene carolina', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (18, true, 'https://picsum.photos/id/794/200/300', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 6, 'a', 9, 'a', 212, 5, 10, 'Sus scrofa', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (19, true, 'https://picsum.photos/id/978/200/300', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 5, 'a', 9, 'b', 389, 21, 11, 'Heloderma horridum', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (20, true, 'https://picsum.photos/id/685/200/300', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', 4, 'c', 6, 'c', 207, 14, 8, 'Casmerodius albus', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (21, true, 'https://picsum.photos/id/140/200/300', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', 5, 'b', 9, 'a', 193, 14, 10, 'Tachybaptus ruficollis', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (22, true, 'https://picsum.photos/id/344/200/300', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', 3, 'b', 6, 'a', 98, 35, 16, 'Terrapene carolina', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (23, true, 'https://picsum.photos/id/827/200/300', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 6, 'c', 7, 'b', 102, 14, 0, 'Sceloporus magister', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (24, true, 'https://picsum.photos/id/799/200/300', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 3, 'a', 7, 'b', 201, 13, 15, 'Phoca vitulina', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (25, true, 'https://picsum.photos/id/694/200/300', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 4, 'c', 6, 'a', 464, 27, 9, 'Pelecanus conspicillatus', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (26, true, 'https://picsum.photos/id/551/200/300', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 6, 'c', 9, 'a', 395, 37, 23, 'Paroaria gularis', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (27, true, 'https://picsum.photos/id/162/200/300', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 4, 'a', 9, 'b', 21, 24, 0, 'Haliaetus leucogaster', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (28, true, 'https://picsum.photos/id/564/200/300', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 3, 'a', 8, 'a', 483, 17, 14, 'Chloephaga melanoptera', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (29, true, 'https://picsum.photos/id/810/200/300', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 4, 'b', 7, 'a', 435, 25, 9, 'Spermophilus lateralis', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (30, true, 'https://picsum.photos/id/164/200/300', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', 3, 'b', 8, 'a', 20, 11, 10, 'Laniaurius atrococcineus', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (31, true, 'https://picsum.photos/id/324/200/300', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', 3, 'b', 7, 'a', 24, 24, 12, 'Otocyon megalotis', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (32, true, 'https://picsum.photos/id/842/200/300', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 6, 'c', 6, 'b', 298, 6, 11, 'Felis concolor', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (33, true, 'https://picsum.photos/id/497/200/300', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', 6, 'c', 8, 'b', 223, 47, 15, 'Damaliscus dorcas', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (34, true, 'https://picsum.photos/id/620/200/300', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 4, 'a', 7, 'c', 385, 34, 23, 'Melursus ursinus', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (35, true, 'https://picsum.photos/id/274/200/300', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 5, 'b', 9, 'c', 252, 41, 22, 'Coluber constrictor', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (36, true, 'https://picsum.photos/id/818/200/300', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', 5, 'a', 6, 'c', 157, 26, 10, 'Tachybaptus ruficollis', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (37, true, 'https://picsum.photos/id/253/200/300', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 3, 'b', 6, 'a', 352, 3, 2, 'Francolinus coqui', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (38, true, 'https://picsum.photos/id/643/200/300', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 6, 'a', 6, 'c', 171, 15, 18, 'Tamiasciurus hudsonicus', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (39, true, 'https://picsum.photos/id/141/200/300', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.', 4, 'b', 8, 'c', 135, 30, 20, 'Ovibos moschatus', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (40, true, 'https://picsum.photos/id/555/200/300', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.', 4, 'a', 9, 'c', 486, 5, 15, 'Eremophila alpestris', 4, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (41, true, 'https://picsum.photos/id/637/200/300', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', 4, 'a', 9, 'b', 271, 42, 15, 'Phascogale tapoatafa', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (42, true, 'https://picsum.photos/id/705/200/300', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 5, 'a', 7, 'a', 255, 11, 15, 'Acrantophis madagascariensis', 4, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (43, true, 'https://picsum.photos/id/440/200/300', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', 6, 'a', 9, 'c', 152, 21, 11, 'Creagrus furcatus', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (44, true, 'https://picsum.photos/id/981/200/300', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 4, 'a', 7, 'b', 467, 28, 23, 'Milvus migrans', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (45, true, 'https://picsum.photos/id/208/200/300', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 3, 'a', 7, 'a', 445, 48, 21, 'Coluber constrictor foxii', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (46, true, 'https://picsum.photos/id/239/200/300', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 4, 'a', 6, 'b', 101, 24, 15, 'Panthera leo', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (47, true, 'https://picsum.photos/id/431/200/300', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 6, 'a', 7, 'b', 133, 25, 12, 'Deroptyus accipitrinus', 4, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (48, true, 'https://picsum.photos/id/379/200/300', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 4, 'c', 7, 'c', 286, 50, 22, 'Milvus migrans', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (49, true, 'https://picsum.photos/id/411/200/300', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 3, 'b', 9, 'c', 409, 28, 23, 'Haliaetus leucogaster', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (50, true, 'https://picsum.photos/id/136/200/300', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 6, 'a', 9, 'b', 35, 10, 3, 'Spermophilus parryii', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (51, true, 'https://picsum.photos/id/330/200/300', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 6, 'a', 9, 'a', 154, 21, 24, 'Ictalurus furcatus', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (52, true, 'https://picsum.photos/id/376/200/300', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', 6, 'a', 7, 'a', 332, 28, 18, 'Tamandua tetradactyla', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (53, true, 'https://picsum.photos/id/492/200/300', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 3, 'a', 6, 'c', 306, 19, 7, 'Rhea americana', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (54, true, 'https://picsum.photos/id/397/200/300', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 3, 'c', 6, 'a', 267, 16, 2, 'Mirounga leonina', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (55, true, 'https://picsum.photos/id/975/200/300', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 4, 'c', 9, 'c', 172, 10, 12, 'Ctenophorus ornatus', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (56, true, 'https://picsum.photos/id/231/200/300', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', 3, 'b', 7, 'a', 199, 9, 2, 'Sylvilagus floridanus', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (57, true, 'https://picsum.photos/id/526/200/300', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.', 3, 'c', 9, 'b', 151, 28, 1, 'unavailable', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (58, true, 'https://picsum.photos/id/161/200/300', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', 3, 'a', 9, 'b', 372, 25, 6, 'Bos taurus', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (59, true, 'https://picsum.photos/id/276/200/300', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 3, 'b', 8, 'b', 311, 19, 15, 'Streptopelia senegalensis', 4, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (60, true, 'https://picsum.photos/id/965/200/300', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 4, 'a', 6, 'b', 389, 47, 16, 'Trichosurus vulpecula', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (61, true, 'https://picsum.photos/id/782/200/300', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', 4, 'a', 7, 'c', 381, 19, 24, 'Columba palumbus', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (62, true, 'https://picsum.photos/id/364/200/300', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', 4, 'a', 9, 'b', 70, 46, 8, 'Ara macao', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (63, true, 'https://picsum.photos/id/460/200/300', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', 6, 'b', 6, 'c', 83, 10, 22, 'Falco mexicanus', 4, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (64, true, 'https://picsum.photos/id/911/200/300', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 4, 'a', 9, 'b', 498, 3, 16, 'Otaria flavescens', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (65, true, 'https://picsum.photos/id/486/200/300', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 3, 'a', 6, 'b', 181, 15, 16, 'Chlidonias leucopterus', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (66, true, 'https://picsum.photos/id/250/200/300', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', 4, 'c', 7, 'a', 435, 27, 18, 'Hymenolaimus malacorhynchus', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (67, true, 'https://picsum.photos/id/314/200/300', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', 5, 'a', 6, 'a', 348, 8, 6, 'Dusicyon thous', 4, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (68, true, 'https://picsum.photos/id/640/200/300', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 5, 'c', 8, 'c', 152, 4, 6, 'Gyps bengalensis', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (69, true, 'https://picsum.photos/id/445/200/300', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 4, 'c', 6, 'c', 189, 5, 3, 'Gymnorhina tibicen', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (70, true, 'https://picsum.photos/id/683/200/300', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 4, 'b', 6, 'b', 165, 10, 13, 'Galago crassicaudataus', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (71, true, 'https://picsum.photos/id/924/200/300', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 4, 'c', 8, 'c', 391, 40, 1, 'Dicrurus adsimilis', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (72, true, 'https://picsum.photos/id/174/200/300', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 6, 'c', 6, 'a', 111, 4, 2, 'Alcelaphus buselaphus caama', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (73, true, 'https://picsum.photos/id/617/200/300', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 5, 'b', 8, 'c', 416, 28, 1, 'Callorhinus ursinus', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (74, true, 'https://picsum.photos/id/362/200/300', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', 3, 'b', 9, 'a', 287, 18, 3, 'Columba palumbus', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (75, true, 'https://picsum.photos/id/675/200/300', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 3, 'b', 8, 'a', 41, 30, 15, 'Heloderma horridum', 4, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (76, true, 'https://picsum.photos/id/173/200/300', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 6, 'c', 9, 'c', 31, 48, 8, 'Suricata suricatta', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (77, true, 'https://picsum.photos/id/457/200/300', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', 5, 'c', 9, 'b', 325, 3, 3, 'Theropithecus gelada', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (78, true, 'https://picsum.photos/id/794/200/300', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 3, 'b', 8, 'b', 319, 42, 7, 'Acrantophis madagascariensis', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (79, true, 'https://picsum.photos/id/170/200/300', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 5, 'a', 9, 'b', 31, 35, 11, 'Eudyptula minor', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (80, true, 'https://picsum.photos/id/964/200/300', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 4, 'b', 8, 'c', 78, 35, 1, 'Cercatetus concinnus', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (81, true, 'https://picsum.photos/id/141/200/300', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 6, 'a', 6, 'b', 46, 32, 14, 'Zalophus californicus', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (82, true, 'https://picsum.photos/id/530/200/300', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', 6, 'b', 7, 'c', 15, 48, 5, 'Tachyglossus aculeatus', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (83, true, 'https://picsum.photos/id/413/200/300', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 3, 'b', 8, 'c', 108, 48, 17, 'Cynictis penicillata', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (84, true, 'https://picsum.photos/id/139/200/300', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 5, 'a', 6, 'a', 23, 20, 1, 'Bubalus arnee', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (85, true, 'https://picsum.photos/id/907/200/300', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 4, 'b', 8, 'a', 140, 46, 19, 'Himantopus himantopus', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (86, true, 'https://picsum.photos/id/304/200/300', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 4, 'c', 8, 'a', 215, 34, 7, 'Ara chloroptera', 4, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (87, true, 'https://picsum.photos/id/113/200/300', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 5, 'a', 9, 'b', 63, 15, 10, 'Ardea cinerea', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (88, true, 'https://picsum.photos/id/892/200/300', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 5, 'c', 9, 'c', 23, 43, 22, 'Alcelaphus buselaphus caama', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (89, true, 'https://picsum.photos/id/497/200/300', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.', 3, 'a', 9, 'c', 50, 9, 4, 'Dendrocitta vagabunda', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (90, true, 'https://picsum.photos/id/366/200/300', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 6, 'c', 9, 'a', 206, 42, 20, 'Francolinus coqui', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (91, true, 'https://picsum.photos/id/569/200/300', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', 4, 'b', 7, 'b', 467, 12, 20, 'Zosterops pallidus', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (92, true, 'https://picsum.photos/id/873/200/300', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 6, 'b', 8, 'b', 122, 19, 3, 'Ardea golieth', 3, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (93, true, 'https://picsum.photos/id/417/200/300', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 4, 'c', 7, 'a', 380, 40, 21, 'Phalacrocorax varius', 4, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (94, true, 'https://picsum.photos/id/338/200/300', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 4, 'a', 7, 'b', 8, 46, 10, 'Eudyptula minor', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (95, true, 'https://picsum.photos/id/389/200/300', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 4, 'c', 7, 'a', 180, 36, 24, 'Ammospermophilus nelsoni', 1, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (96, true, 'https://picsum.photos/id/250/200/300', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 5, 'b', 9, 'a', 272, 49, 13, 'Nesomimus trifasciatus', 4, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (97, true, 'https://picsum.photos/id/343/200/300', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 4, 'b', 7, 'b', 282, 38, 0, 'Macropus robustus', 2, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (98, true, 'https://picsum.photos/id/631/200/300', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 4, 'c', 8, 'b', 354, 5, 17, 'Phacochoerus aethiopus', 0, null);
insert into topo (id, available, cover_image, description, diff_max, diff_max_sub, diff_min, diff_min_sub, height, num_ascents, site_id, title, created_id, taker_id) values (99, true, 'https://picsum.photos/id/620/200/300', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 4, 'b', 7, 'a', 203, 40, 19, 'Carduelis uropygialis', 0, null);

-- Generate Sectors
insert into sector (id, ascent_image, name, site_id) values (0, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Francolinus leucoscepus', 11);
insert into sector (id, ascent_image, name, site_id) values (1, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Sarcorhamphus papa', 11);
insert into sector (id, ascent_image, name, site_id) values (2, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Leprocaulinus vipera', 4);
insert into sector (id, ascent_image, name, site_id) values (3, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Leptoptilos crumeniferus', 17);
insert into sector (id, ascent_image, name, site_id) values (4, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Trichosurus vulpecula', 17);
insert into sector (id, ascent_image, name, site_id) values (5, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Phaethon aethereus', 17);
insert into sector (id, ascent_image, name, site_id) values (6, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Passer domesticus', 4);
insert into sector (id, ascent_image, name, site_id) values (7, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Amblyrhynchus cristatus', 10);
insert into sector (id, ascent_image, name, site_id) values (8, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Tauraco porphyrelophus', 19);
insert into sector (id, ascent_image, name, site_id) values (9, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Colobus guerza', 20);
insert into sector (id, ascent_image, name, site_id) values (10, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Plocepasser mahali', 21);
insert into sector (id, ascent_image, name, site_id) values (11, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Aonyx cinerea', 21);
insert into sector (id, ascent_image, name, site_id) values (12, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Columba livia', 5);
insert into sector (id, ascent_image, name, site_id) values (13, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Uraeginthus granatina', 12);
insert into sector (id, ascent_image, name, site_id) values (14, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'unavailable', 22);
insert into sector (id, ascent_image, name, site_id) values (15, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Varanus salvator', 13);
insert into sector (id, ascent_image, name, site_id) values (16, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Ardea golieth', 21);
insert into sector (id, ascent_image, name, site_id) values (17, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Anas bahamensis', 18);
insert into sector (id, ascent_image, name, site_id) values (18, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Parus atricapillus', 2);
insert into sector (id, ascent_image, name, site_id) values (19, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'unavailable', 10);
insert into sector (id, ascent_image, name, site_id) values (20, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Dasypus novemcinctus', 15);
insert into sector (id, ascent_image, name, site_id) values (21, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Sagittarius serpentarius', 12);
insert into sector (id, ascent_image, name, site_id) values (22, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Lorythaixoides concolor', 16);
insert into sector (id, ascent_image, name, site_id) values (23, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Anitibyx armatus', 0);
insert into sector (id, ascent_image, name, site_id) values (24, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Ramphastos tucanus', 20);
insert into sector (id, ascent_image, name, site_id) values (25, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Platalea leucordia', 6);
insert into sector (id, ascent_image, name, site_id) values (26, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Corythornis cristata', 16);
insert into sector (id, ascent_image, name, site_id) values (27, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Dasypus septemcincus', 13);
insert into sector (id, ascent_image, name, site_id) values (28, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Junonia genoveua', 17);
insert into sector (id, ascent_image, name, site_id) values (29, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Tockus erythrorhyncus', 0);
insert into sector (id, ascent_image, name, site_id) values (30, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Cercatetus concinnus', 4);
insert into sector (id, ascent_image, name, site_id) values (31, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Certotrichas paena', 2);
insert into sector (id, ascent_image, name, site_id) values (32, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Dusicyon thous', 3);
insert into sector (id, ascent_image, name, site_id) values (33, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Ara macao', 1);
insert into sector (id, ascent_image, name, site_id) values (34, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Aonyx cinerea', 7);
insert into sector (id, ascent_image, name, site_id) values (35, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Tamiasciurus hudsonicus', 19);
insert into sector (id, ascent_image, name, site_id) values (36, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Eurocephalus anguitimens', 9);
insert into sector (id, ascent_image, name, site_id) values (37, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Semnopithecus entellus', 16);
insert into sector (id, ascent_image, name, site_id) values (38, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Certotrichas paena', 23);
insert into sector (id, ascent_image, name, site_id) values (39, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Acrantophis madagascariensis', 10);
insert into sector (id, ascent_image, name, site_id) values (40, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Acridotheres tristis', 18);
insert into sector (id, ascent_image, name, site_id) values (41, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Otocyon megalotis', 8);
insert into sector (id, ascent_image, name, site_id) values (42, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Psophia viridis', 21);
insert into sector (id, ascent_image, name, site_id) values (43, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Bettongia penicillata', 8);
insert into sector (id, ascent_image, name, site_id) values (44, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Mustela nigripes', 21);
insert into sector (id, ascent_image, name, site_id) values (45, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Oreamnos americanus', 21);
insert into sector (id, ascent_image, name, site_id) values (46, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Philetairus socius', 20);
insert into sector (id, ascent_image, name, site_id) values (47, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Sterna paradisaea', 4);
insert into sector (id, ascent_image, name, site_id) values (48, 'http://www.parcregional.com/wp-content/uploads/2015/10/Topo-cesaire-duo.jpg', 'Macropus eugenii', 23);
insert into sector (id, ascent_image, name, site_id) values (49, 'http://static.apidae-tourisme.com/filestore/objets-touristiques/images/175/7/591791-diaporama.jpg', 'Mungos mungo', 1);

-- Generate Ascents
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (1, 9, 69, 'Stork, saddle-billed', 6.7, 'b', 21);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (2, 9, 91, 'Mouse, four-striped grass', 7.8, 'a', 18);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (3, 8, 26, 'Mallard', 6.8, 'c', 23);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (4, 6, 40, 'Bandicoot, short-nosed', 3.2, 'c', 1);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (5, 8, 93, 'Ferruginous hawk', 2.1, 'b', 36);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (6, 8, 80, 'Tortoise, desert', 4.8, 'b', 35);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (7, 3, 48, 'Eurasian hoopoe', 4.6, 'b', 32);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (8, 6, 86, 'Vulture, turkey', 6.4, 'c', 33);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (9, 9, 67, 'Galapagos tortoise', 2.6, 'b', 22);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (10, 6, 86, 'Springbok', 9.9, 'b', 15);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (11, 8, 58, 'Gerenuk', 6.7, 'a', 16);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (12, 3, 14, 'Gnu, brindled', 4.4, 'c', 6);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (13, 4, 84, 'Guerza', 7.4, 'b', 35);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (14, 3, 97, 'Cottonmouth', 4.7, 'a', 17);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (15, 9, 99, 'Kangaroo, black-faced', 5.8, 'b', 46);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (16, 8, 64, 'Hyena, striped', 5.3, 'b', 44);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (17, 3, 13, 'Leopard', 5.4, 'c', 9);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (18, 7, 20, 'White-rumped vulture', 8.7, 'a', 36);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (19, 3, 11, 'Bustard, denham''s', 5.7, 'b', 25);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (20, 3, 53, 'Dove, laughing', 5.6, 'b', 39);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (21, 3, 81, 'Blue crane', 2.8, 'c', 18);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (22, 6, 27, 'Sheep, stone', 1.1, 'c', 46);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (23, 3, 84, 'Cormorant, little', 1.7, 'b', 39);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (24, 3, 64, 'Blue racer', 4.5, 'a', 48);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (25, 4, 39, 'Wolf, timber', 1.2, 'a', 40);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (26, 8, 12, 'Magistrate black colobus', 7.3, 'a', 32);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (27, 5, 82, 'White-winged tern', 1.1, 'b', 39);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (28, 3, 52, 'Bird, red-billed tropic', 2.9, 'b', 23);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (29, 3, 43, 'Bear, grizzly', 6.1, 'b', 19);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (30, 3, 60, 'Deer, swamp', 5.6, 'c', 28);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (31, 5, 82, 'Tern, arctic', 8.7, 'c', 14);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (32, 4, 44, 'Common wolf', 4.7, 'c', 7);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (33, 7, 83, 'Elk, Wapiti', 3.1, 'a', 47);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (34, 3, 65, 'Heron, giant', 4.2, 'c', 43);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (35, 3, 17, 'Frilled lizard', 4.2, 'a', 43);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (36, 9, 57, 'Hare, arctic', 4.5, 'c', 4);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (37, 6, 38, 'Egret, great', 8.4, 'a', 6);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (38, 6, 94, 'Penguin, little blue', 5.7, 'c', 1);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (39, 8, 46, 'Kelp gull', 7.0, 'a', 45);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (40, 7, 39, 'Sandgrouse, yellow-throated', 1.5, 'c', 45);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (41, 4, 91, 'Wambenger, red-tailed', 9.0, 'a', 43);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (42, 7, 60, 'Asiatic wild ass', 1.5, 'c', 12);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (43, 6, 76, 'Phalarope, red', 4.9, 'c', 25);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (44, 7, 17, 'Racer, american', 7.2, 'c', 10);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (45, 3, 39, 'Dragon, netted rock', 2.5, 'c', 25);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (46, 9, 53, 'Black-crowned night heron', 1.3, 'c', 40);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (47, 7, 82, 'Pelican, eastern white', 9.0, 'b', 40);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (48, 3, 47, 'Partridge, coqui', 6.8, 'a', 23);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (49, 4, 93, 'Turkey, wild', 8.8, 'b', 41);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (50, 9, 62, 'Dog, african wild', 1.8, 'c', 32);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (51, 9, 60, 'Egret, snowy', 5.3, 'a', 26);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (52, 6, 60, 'Pampa gray fox', 1.1, 'a', 1);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (53, 7, 19, 'Gull, southern black-backed', 7.8, 'b', 24);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (54, 4, 32, 'Cormorant, little', 2.4, 'a', 49);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (55, 8, 80, 'Two-toed tree sloth', 9.1, 'c', 18);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (56, 3, 50, 'Springbok', 5.3, 'b', 24);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (57, 5, 21, 'Tiger snake', 6.3, 'c', 7);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (58, 5, 98, 'Wagtail, african pied', 2.5, 'c', 17);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (59, 5, 64, 'Hanuman langur', 3.3, 'a', 38);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (60, 4, 57, 'Bat, madagascar fruit', 6.7, 'c', 26);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (61, 8, 72, 'Dolphin, striped', 8.6, 'c', 23);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (62, 3, 23, 'Denham''s bustard', 3.7, 'c', 47);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (63, 9, 24, 'Burchell''s gonolek', 3.2, 'c', 28);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (64, 4, 19, 'Violet-crested turaco', 8.2, 'b', 3);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (65, 3, 45, 'Moccasin, water', 7.0, 'c', 26);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (66, 7, 12, 'North American porcupine', 9.2, 'b', 36);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (67, 6, 10, 'Asian red fox', 3.7, 'a', 34);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (68, 9, 62, 'Great skua', 4.5, 'c', 33);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (69, 7, 29, 'Topi', 2.0, 'b', 18);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (70, 5, 85, 'Eagle, pallas''s fish', 4.7, 'c', 36);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (71, 6, 37, 'Clark''s nutcracker', 8.5, 'b', 28);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (72, 9, 86, 'Butterfly, tropical buckeye', 2.7, 'b', 28);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (73, 8, 40, 'Weaver, white-browed sparrow', 4.0, 'a', 38);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (74, 9, 86, 'Springhare', 5.5, 'c', 33);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (75, 4, 28, 'Baboon, yellow', 3.6, 'b', 18);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (76, 4, 14, 'Dragon, asian water', 3.3, 'c', 31);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (77, 6, 46, 'Paca', 5.8, 'b', 15);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (78, 4, 47, 'Stanley bustard', 7.3, 'c', 27);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (79, 6, 82, 'Timber wolf', 1.4, 'b', 27);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (80, 9, 41, 'African ground squirrel (unidentified)', 4.7, 'c', 46);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (81, 4, 48, 'Skimmer, four-spotted', 4.7, 'c', 27);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (82, 7, 12, 'Netted rock dragon', 3.8, 'a', 1);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (83, 8, 33, 'Yellow-billed stork', 7.1, 'a', 0);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (84, 7, 31, 'Eleven-banded armadillo (unidentified)', 3.7, 'b', 40);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (85, 7, 80, 'Levaillant''s barbet', 6.8, 'a', 17);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (86, 7, 62, 'Snake, tiger', 9.5, 'c', 31);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (87, 7, 37, 'White-rumped vulture', 5.7, 'c', 17);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (88, 7, 74, 'North American porcupine', 9.0, 'c', 2);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (89, 9, 70, 'Grouse, sage', 4.8, 'b', 17);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (90, 6, 98, 'Bustard, kori', 7.8, 'b', 37);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (91, 5, 66, 'Ring-tailed gecko', 8.0, 'a', 13);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (92, 3, 11, 'Penguin, galapagos', 1.8, 'b', 41);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (93, 6, 14, 'Grey fox', 4.9, 'a', 5);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (94, 4, 44, 'Vulture, griffon', 5.9, 'c', 3);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (95, 7, 61, 'Monkey, red howler', 1.9, 'b', 34);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (96, 7, 31, 'Gnu, brindled', 7.0, 'c', 2);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (97, 8, 28, 'Mongoose, banded', 6.6, 'b', 48);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (98, 6, 50, 'Lizard, blue-tongued', 9.3, 'a', 30);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (99, 4, 39, 'Cat, kaffir', 2.6, 'c', 15);
insert into ascent (id, difficulty, height, name, numeration, sub_difficulty, sector_id) values (100, 3, 66, 'Monkey, bleeding heart', 5.5, 'c', 17);

-- Generate News
insert into news (id, content, creation_date, title, created_id) values (0, 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-07-18 15:02:46.904', 'Digitized user-facing encoding', 1);
insert into news (id, content, creation_date, title, created_id) values (1, 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-08-31 12:09:51.904', 'Switchable needs-based synergy', 0);
insert into news (id, content, creation_date, title, created_id) values (2, 'Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2019-04-19 10:39:40.904', 'Automated bi-directional extranet', 1);
insert into news (id, content, creation_date, title, created_id) values (3, 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-12-27 08:29:28.904', 'Configurable systemic function', 0);
insert into news (id, content, creation_date, title, created_id) values (4, 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-10-21 01:27:11.904', 'Organized full-range help-desk', 1);
insert into news (id, content, creation_date, title, created_id) values (5, 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2018-11-04 04:32:23.904', 'Enterprise-wide 3rd generation internet solution', 1);
insert into news (id, content, creation_date, title, created_id) values (6, 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2019-06-17 10:22:24.904', 'Progressive multimedia intranet', 0);
insert into news (id, content, creation_date, title, created_id) values (7, 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2018-08-10 01:11:54.904', 'Triple-buffered 6th generation flexibility', 0);
insert into news (id, content, creation_date, title, created_id) values (8, 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2019-04-29 22:52:34.904', 'Mandatory secondary ability', 1);
insert into news (id, content, creation_date, title, created_id) values (9, 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-09-14 06:05:07.904', 'Intuitive incremental data-warehouse', 1);

ALTER SEQUENCE hibernate_sequence RESTART WITH 105;