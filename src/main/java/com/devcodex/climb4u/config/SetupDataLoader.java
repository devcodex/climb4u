package com.devcodex.climb4u.config;

import com.devcodex.climb4u.dao.RoleDao;
import com.devcodex.climb4u.dao.TopoDao;
import com.devcodex.climb4u.dao.TopoLendingDao;
import com.devcodex.climb4u.dao.UserAccountDao;
import com.devcodex.climb4u.model.Role;
import com.devcodex.climb4u.model.Topo;
import com.devcodex.climb4u.model.TopoLending;
import com.devcodex.climb4u.model.UserAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {
    private boolean alreadySetup = false;

    @Autowired
    RoleDao roleDao;
    @Autowired
    UserAccountDao userAccountDao;
    @Autowired
    TopoDao topoDao;
    @Autowired
    TopoLendingDao topoLendingDao;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        if(alreadySetup) {
            return;
        }

//        this.initRoles();
//        this.initUsers();
//        this.initTopos();
//        this.initLendings();

        alreadySetup = true;
    }

    private void initRoles() {
        createRoleIfNotFound("ROLE_USER");
        createRoleIfNotFound("ROLE_ADMIN");
        createRoleIfNotFound("ROLE_OFFICIAL");
    }

    private void initUsers() {
        UserAccount userAccount = new UserAccount();
        userAccount.setUsername("DevCodex");
        userAccount.setPassword(passwordEncoder.encode("plainPassword"));
        userAccount.setEmail("devcodex@outlook.com");
        userAccount.setEnabled(true);
        userAccount.setProfileImage("https://upload.wikimedia.org/wikipedia/commons/5/50/RedPandaFullBody.JPG");
        userAccount.setCreationDate(new Timestamp(new Date().getTime()));
        userAccount.setRoles(Arrays.asList(roleDao.findByName("ROLE_USER")));

        UserAccount userAccount2 = new UserAccount();
        userAccount2.setUsername("TestUser");
        userAccount2.setPassword(passwordEncoder.encode("123123"));
        userAccount2.setEmail("test@test.com");
        userAccount2.setEnabled(true);
        userAccount2.setCreationDate(new Timestamp(new Date().getTime()));
        userAccount2.setRoles(Arrays.asList(roleDao.findByName("ROLE_USER")));

        createUserIfNotFound(userAccount);
        createUserIfNotFound(userAccount2);
    }

    private void initTopos() {
//        Topo topo1 = new Topo();
//        topo1.setTitle("Fratercula corniculata");
//        topo1.setSite("Behiund Lyon");
//        topo1.setCoverImage("http://dummyimage.com/140x250.png/cc0000/ffffff");
//        topo1.setDescription("Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.");
//        topo1.setDiffMin(3);
//        topo1.setDiffMinSub("a");
//        topo1.setDiffMax(4);
//        topo1.setDiffMaxSub("c");
//        topo1.setNumAscents(30);
//        topo1.setHeight(300);
//        topo1.setAvailable(true);
//        topo1.setReleaseDate("24/02/2017");
//        topo1.setCreatedBy(userAccountDao.findByUsername("DevCodex"));
//
//        Topo topo2 = new Topo();
//        topo2.setTitle("Larus fuliginosus");
//        topo2.setSite("Mambalan");
//        topo2.setCoverImage("http://dummyimage.com/100x248.jpg/cc0000/ffffff");
//        topo2.setDescription("Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.");
//        topo2.setDiffMin(3);
//        topo2.setDiffMinSub("a");
//        topo2.setDiffMax(6);
//        topo2.setDiffMaxSub("c");
//        topo2.setNumAscents(40);
//        topo2.setHeight(250);
//        topo2.setAvailable(true);
//        topo2.setReleaseDate("05/06/2014");
//        topo2.setCreatedBy(userAccountDao.findByUsername("DevCodex"));
//
//        Topo topo3 = new Topo();
//        topo3.setTitle("Ara macao");
//        topo3.setSite("Alverca do Ribatejo");
//        topo3.setCoverImage("http://dummyimage.com/176x187.png/5fa2dd/ffffff");
//        topo3.setDescription("Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;" +
//                " Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.");
//        topo3.setDiffMin(6);
//        topo3.setDiffMinSub("a");
//        topo3.setDiffMax(9);
//        topo3.setDiffMaxSub("c");
//        topo3.setNumAscents(15);
//        topo3.setHeight(200);
//        topo3.setAvailable(true);
//        topo3.setReleaseDate("03/03/2003");
//        topo3.setCreatedBy(userAccountDao.findByUsername("DevCodex"));
//
//        Topo topo4 = new Topo();
//        topo4.setTitle("Ephippiorhynchus mycteria");
//        topo4.setSite("Uryupinsk");
//        topo4.setCoverImage("http://dummyimage.com/113x108.bmp/5fa2dd/ffffff");
//        topo4.setDescription("In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.");
//        topo4.setDiffMin(3);
//        topo4.setDiffMinSub("a");
//        topo4.setDiffMax(8);
//        topo4.setDiffMaxSub("c");
//        topo4.setNumAscents(45);
//        topo4.setHeight(450);
//        topo4.setAvailable(true);
//        topo4.setReleaseDate("26/12/2006");
//        topo4.setCreatedBy(userAccountDao.findByUsername("DevCodex"));
//
//        Topo topo5 = new Topo();
//        topo5.setTitle("Didelphis virginiana");
//        topo5.setSite("Portet-sur-Garonne");
//        topo5.setCoverImage("http://dummyimage.com/150x172.png/cc0000/ffffff");
//        topo5.setDescription("Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.");
//        topo5.setDiffMin(7);
//        topo5.setDiffMinSub("a");
//        topo5.setDiffMax(9);
//        topo5.setDiffMaxSub("c");
//        topo5.setNumAscents(5);
//        topo5.setHeight(200);
//        topo5.setAvailable(true);
//        topo5.setReleaseDate("05/06/2014");
//        topo5.setCreatedBy(userAccountDao.findByUsername("DevCodex"));

//        createTopoIfNotFound(topo1);
//        createTopoIfNotFound(topo2);
//        createTopoIfNotFound(topo3);
//        createTopoIfNotFound(topo4);
//        createTopoIfNotFound(topo5);

    }

    private void initLendings() {
        TopoLending lending1 = new TopoLending();
        lending1.setAnswered(false);
        lending1.setTaker(userAccountDao.findByUsername("TestUser"));
        lending1.setTopo(topoDao.findByTitle("Didelphis virginiana"));
        lending1.setCreationDate(new Timestamp(new Date().getTime()));

        TopoLending lending2 = new TopoLending();
        lending2.setAnswered(true);
        lending2.setAccepted(true);
        lending2.setTaker(userAccountDao.findByUsername("TestUser"));
        lending2.setTopo(topoDao.findByTitle("Didelphis virginiana"));
        lending2.setCreationDate(new Timestamp(new Date().getTime()));

        TopoLending lending3 = new TopoLending();
        lending3.setAnswered(true);
        lending3.setAccepted(false);
        lending3.setTaker(userAccountDao.findByUsername("TestUser"));
        lending3.setTopo(topoDao.findByTitle("Didelphis virginiana"));
        lending3.setCreationDate(new Timestamp(new Date().getTime()));

        topoLendingDao.save(lending1);
        topoLendingDao.save(lending2);
        topoLendingDao.save(lending3);
    }

    private void createRoleIfNotFound(String name) {
        Role role = roleDao.findByName(name);
        if (role == null) {
            roleDao.save(new Role(name));
        }
    }

    private void createUserIfNotFound(UserAccount userAccount) {
        if(userAccountDao.findByUsername(userAccount.getUsername()) == null) {
            userAccountDao.save(userAccount);
        }
    }

    private void createTopoIfNotFound(Topo topo) {
        if(topoDao.findByTitle(topo.getTitle()) == null) {
            topoDao.save(topo);
        }
    }
}
