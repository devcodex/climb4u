package com.devcodex.climb4u;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Climb4uApplication {

	public static void main(String[] args) {
		SpringApplication.run(Climb4uApplication.class, args);
	}
}
