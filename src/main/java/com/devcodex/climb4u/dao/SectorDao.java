package com.devcodex.climb4u.dao;

import com.devcodex.climb4u.model.Sector;
import com.devcodex.climb4u.model.Site;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface SectorDao extends JpaRepository<Sector, Integer> {
    ArrayList<Sector> findBySite(Site site);
}
