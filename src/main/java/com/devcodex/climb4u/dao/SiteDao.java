package com.devcodex.climb4u.dao;

import com.devcodex.climb4u.model.Site;
import com.devcodex.climb4u.model.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.ArrayList;
import java.util.List;

public interface SiteDao extends JpaRepository<Site, Integer>, JpaSpecificationExecutor {
    Site findByName(String name);
    Site findById(int id);
    List<Site> findAllByOrderByName();
    ArrayList<Site> findAllByAvailableOrderByName(boolean available);
    ArrayList<Site> findAllByCreatedBy(UserAccount userAccount);
    Long countAllBy();
}
