package com.devcodex.climb4u.dao;

import com.devcodex.climb4u.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleDao extends JpaRepository<Role, Integer> {
    Role findByName(String name);
}
