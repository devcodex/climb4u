package com.devcodex.climb4u.dao;

import com.devcodex.climb4u.model.Topo;
import com.devcodex.climb4u.model.TopoLending;
import com.devcodex.climb4u.model.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.lang.reflect.Array;
import java.util.ArrayList;

public interface TopoLendingDao extends JpaRepository<TopoLending, Integer> {
    ArrayList<TopoLending> findAllByTopo(Topo topo);
    ArrayList<TopoLending> findAllByTakerAndTopo(UserAccount userAccount, Topo topo);
    ArrayList<TopoLending> findAllByTaker(UserAccount userAccount);
}
