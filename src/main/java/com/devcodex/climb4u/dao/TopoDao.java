package com.devcodex.climb4u.dao;

import com.devcodex.climb4u.model.Topo;
import com.devcodex.climb4u.model.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.ArrayList;

public interface TopoDao extends JpaRepository<Topo, Integer>, JpaSpecificationExecutor {
    Topo findByTitle(String title);
    Topo findById(int id);
    Long countAllByAvailable(boolean available);
    ArrayList<Topo> findAllByAvailableOrderByTitle(boolean available);
    ArrayList<Topo> findAllByCreatedBy(UserAccount userAccount);
}
