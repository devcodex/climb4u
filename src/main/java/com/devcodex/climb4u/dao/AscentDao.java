package com.devcodex.climb4u.dao;

import com.devcodex.climb4u.model.Ascent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AscentDao extends JpaRepository<Ascent, Integer> {
}
