package com.devcodex.climb4u.dao;

import com.devcodex.climb4u.model.Topo;
import com.devcodex.climb4u.model.TopoReview;
import com.devcodex.climb4u.model.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface TopoReviewDao extends JpaRepository<TopoReview, Integer> {
    ArrayList<TopoReview> findAllByTopo(Topo topo);
    TopoReview findByTopoAndCreatedBy(Topo topo, UserAccount userAccount);
}
