package com.devcodex.climb4u.dao;

import com.devcodex.climb4u.model.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAccountDao extends JpaRepository<UserAccount, Integer> {
    UserAccount findByEmail(String email);
    UserAccount findByUsername(String username);
}
