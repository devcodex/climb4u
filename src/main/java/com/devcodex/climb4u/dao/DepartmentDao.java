package com.devcodex.climb4u.dao;

import com.devcodex.climb4u.model.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentDao extends JpaRepository<Department, Integer> {
}
