package com.devcodex.climb4u.dao;

import com.devcodex.climb4u.model.Site;
import com.devcodex.climb4u.model.SiteComment;
import com.devcodex.climb4u.model.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.ArrayList;
import java.util.List;

public interface SiteCommentDao extends JpaRepository<SiteComment, Integer>, JpaSpecificationExecutor {
    ArrayList<SiteComment> findAllBySiteOrderByCreationDate(Site site);
}
