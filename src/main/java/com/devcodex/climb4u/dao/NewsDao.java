package com.devcodex.climb4u.dao;

import com.devcodex.climb4u.model.News;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface NewsDao extends JpaRepository<News, Integer> {
    ArrayList<News> findAllByOrderByCreationDateDesc();
}
