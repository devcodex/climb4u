package com.devcodex.climb4u.model.dto;

import com.devcodex.climb4u.model.Site;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class SiteDTO {
    private Integer siteId;
    private String name;
    private String official;
    private Integer departmentId;
    private String type;
    private String coverImage;
    private Integer height;
    private Integer diffMin;
    private String diffMinSub;
    private Integer diffMax;
    private String diffMaxSub;
    private String location;
    private String description;
    private String accessNotes;
    private boolean init;

    public SiteDTO() {
        this.name = "";
        this.siteId = null;
        this.official = "";
        this.departmentId = null;
        this.coverImage = "";
        this.height = 0;
        this.diffMin = 0;
        this.diffMinSub = "a";
        this.diffMax = 0;
        this.diffMaxSub = "a";
        this.location = "";
        this.description = "";
        this.accessNotes = "";
        this.type = "";
    }

    public SiteDTO(Site site) {
        this.siteId = site.getId();
        this.name = site.getName();
        this.siteId = site.getId();
        this.departmentId = site.getDepartment().getId();
        this.coverImage = site.getCoverImage();
        this.height = site.getHeight();
        this.location = site.getLocation();
        this.description = site.getDescription();
        this.accessNotes = site.getAccessNotes();
        this.type = site.getType();
    }
}
