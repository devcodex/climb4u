package com.devcodex.climb4u.model.dto;

import com.devcodex.climb4u.validator.annotation.PasswordMatches;
import com.devcodex.climb4u.validator.annotation.ValidEmail;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@PasswordMatches
@Getter @Setter @ToString
public class UserAccountDTO {
    @NotNull
    @NotEmpty
    private String username;

    @ValidEmail
    @NotNull
    @NotEmpty
    private String email;
    private String emailConfirmation;

    @NotNull
    @NotEmpty
    private String password;
    private String passwordConfirmation;
}
