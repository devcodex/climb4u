package com.devcodex.climb4u.model.dto;

import com.devcodex.climb4u.model.Sector;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@NoArgsConstructor
public class SectorDTO {
    private int sectorId;
    private String name;
    private int numAscents;
    private String diffMin;
    private String diffSubMin;
    private String diffMax;
    private String diffSubMax;

    public SectorDTO(Sector sector) {
        this.sectorId = sector.getId();
        this.name = sector.getName();
    }
}
