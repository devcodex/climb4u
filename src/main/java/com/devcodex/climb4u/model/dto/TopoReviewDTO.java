package com.devcodex.climb4u.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TopoReviewDTO {
    private String title;

    private int value;

    private String details;

    private int topoId;
}
