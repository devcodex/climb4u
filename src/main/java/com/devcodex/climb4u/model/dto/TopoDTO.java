package com.devcodex.climb4u.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter @Setter @ToString
public class TopoDTO implements Serializable {
    private String title;
    private Integer site;
    private Integer minAscent;
    private Integer maxAscent;
    private Integer minDifficulty;
    private String minDifficultySub;
    private Integer maxDifficulty;
    private String maxDifficultySub;
    private Integer minHeight;
    private Integer maxHeight;
    private boolean available;
    private boolean init;

    public TopoDTO() {
        this.setTitle("");
        this.setMinAscent(0);
        this.setMaxAscent(9999);
        this.setMinDifficulty(3);
        this.setMaxDifficulty(9);
        this.setMinHeight(0);
        this.setMaxHeight(9999);
        this.setAvailable(true);
        this.setInit(true);
    }
}
