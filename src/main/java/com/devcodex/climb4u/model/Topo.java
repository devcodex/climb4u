package com.devcodex.climb4u.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity @Table(name = "topo")
@Getter @Setter @ToString
public class Topo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String title;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String description;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String coverImage;

    @ManyToOne
    @JoinColumn(name = "site_id")
    private Site site;

    private int diffMin;
    private String diffMinSub;

    private int diffMax;
    private String diffMaxSub;

    private int height;

    private int numAscents;

    private String releaseDate;

    @ManyToOne
    @JoinColumn(name = "created_id")
    private UserAccount createdBy;

    @ManyToOne
    @JoinColumn(name = "taker_id")
    private UserAccount takenBy;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "topo")
    private Set<TopoLending> topoLendings = new HashSet<>();

    private boolean available;

    public boolean checkValidReleaseDate() {
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        format.setLenient(false);
        try {
            Date maxDate = format.parse("01/01/2099");
            if(format.parse(this.releaseDate).after(maxDate)) {
                throw new ParseException("Date goes beyond humanity's existence...", 0);
            }
            format.parse(this.releaseDate);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    public boolean checkValidDifficulties() {
        if(this.diffMin > this.diffMax) {
            return false;
        } else if(Character.getNumericValue(this.diffMinSub.charAt(0)) > Character.getNumericValue(this.diffMaxSub.charAt(0))) {
            return false;
        } else {
            return true;
        }
    }
}