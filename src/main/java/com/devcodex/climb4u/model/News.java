package com.devcodex.climb4u.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

@Entity @Table(name = "news")
@Getter @Setter @ToString
public class News {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String title;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String content;

    private Date creationDate;

    @ManyToOne
    @JoinColumn(name = "created_id")
    private UserAccount createdBy;
}
