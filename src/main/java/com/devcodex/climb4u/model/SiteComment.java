package com.devcodex.climb4u.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity @Table
@Getter @Setter @ToString
public class SiteComment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotNull
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String text;

    private Date creationDate;

    private Date lastModified;

    @ManyToOne
    @JoinColumn(name = "edited_id")
    private UserAccount lastEditedBy;

    private String modification_reason;

    private boolean removed;

    @ManyToOne
    @JoinColumn(name = "created_id")
    private UserAccount createdBy;

    @ManyToOne
    @JoinColumn(name = "sector_id")
    private Site site;
}
