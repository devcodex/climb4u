package com.devcodex.climb4u.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity @Table(name = "ascent")
@Getter @Setter @ToString
public class Ascent {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private float numeration;

    private String name;

    private int difficulty;
    private String subDifficulty;

    private int height;

    @ManyToOne
    @JoinColumn(name = "sector_id")
    private Sector sector;
}