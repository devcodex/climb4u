package com.devcodex.climb4u.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity @Table
@Getter @Setter @ToString
public class TopoReview {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String title;

    private int value;

    private String details;

    @ManyToOne
    @JoinColumn(name = "created_id")
    private UserAccount createdBy;

    @ManyToOne
    @JoinColumn(name = "topo_id")
    private Topo topo;
}
