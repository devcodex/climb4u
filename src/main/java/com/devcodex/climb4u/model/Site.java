package com.devcodex.climb4u.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity @Table
@Getter @Setter @ToString(exclude = "sectorList")
public class Site {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;

    private int height;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String accessNotes;

    private boolean official;

    private String type;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String coverImage;

    //URL containing google maps embedded iframe
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String location;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String description;

    private boolean available;

    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;

    @ManyToOne
    @JoinColumn(name = "created_id")
    private UserAccount createdBy;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "site")
    private List<Sector> sectorList;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "site")
    private Set<Topo> topoList = new HashSet<>();

    public String getMinDiff() {
        if(this.sectorList.size() == 0) {
            return "n/a";
        }
        boolean foundAscents = false;
        int diffMin = 9;
        char diffMinSub = 'c';
        for(Sector sector : this.sectorList) {
            if(sector.getAscentList().size() != 0) {
                foundAscents = true;
                if (diffMin > Character.getNumericValue(sector.getMinDiff().charAt(0))) {
                    diffMin = Character.getNumericValue(sector.getMinDiff().charAt(0));
                    diffMinSub = sector.getMinDiff().charAt(1);
                } else if(diffMin == Character.getNumericValue(sector.getMinDiff().charAt(0))
                        && Character.getNumericValue(diffMinSub) > Character.getNumericValue(sector.getMinDiff().charAt(1))) {
                    diffMinSub = sector.getMinDiff().charAt(1);
                }
            }
        }
        if(!foundAscents) {
            return "n/a";
        }
        return "" + diffMin + diffMinSub;
    }

    public String getMaxDiff() {
        if(this.sectorList.size() == 0) {
            return "n/a";
        }
        boolean foundAscents = false;
        int diffMax = 3;
        char diffMaxSub = 'a';
        for(Sector sector : this.sectorList) {
            if(sector.getAscentList().size() != 0) {
                foundAscents = true;
                if (diffMax < Character.getNumericValue(sector.getMaxDiff().charAt(0))) {
                    diffMax = Character.getNumericValue(sector.getMaxDiff().charAt(0));
                    diffMaxSub = sector.getMaxDiff().charAt(1);
                } else if(diffMax == Character.getNumericValue(sector.getMaxDiff().charAt(0))
                        && Character.getNumericValue(diffMaxSub) > Character.getNumericValue(sector.getMaxDiff().charAt(1))) {
                    diffMaxSub = sector.getMaxDiff().charAt(1);
                }
            }
        }
        if(!foundAscents) {
            return "n/a";
        }
        return "" + diffMax + diffMaxSub;
    }
}