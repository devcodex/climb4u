package com.devcodex.climb4u.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity @Table(name = "topo_lending")
@Getter @Setter
public class TopoLending {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "topo_id")
    private Topo topo;

    @ManyToOne
    @JoinColumn(name = "taker_id")
    private UserAccount taker;

    @NotNull
    private Date creationDate;

    private boolean answered;

    private boolean accepted;
}
