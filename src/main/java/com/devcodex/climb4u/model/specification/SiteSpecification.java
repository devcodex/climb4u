package com.devcodex.climb4u.model.specification;

import com.devcodex.climb4u.model.Ascent;
import com.devcodex.climb4u.model.Sector;
import com.devcodex.climb4u.model.Site;
import com.devcodex.climb4u.model.Topo;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class SiteSpecification {

    public static Specification<Site> siteIdEquals(Integer siteId) {
        return (root, query, cb) -> siteId == null ? null : cb.equal(root.get("id"), siteId);
    }

    public static Specification<Site> siteDepartmentIdEquals(Integer departmentId) {
        return (root, query, cb) -> departmentId == null ? null : cb.equal(root.get("department").get("id"), departmentId);
    }

    public static Specification<Site> siteTypeEquals(String type) {
        return (root, query, cb) -> type.equals("") ? null : cb.equal(root.get("type"), type);
    }

    public static Specification<Site> siteHeightGreaterOrEqualTo(Integer height) {
        return (root, query, cb) -> height == null ? null : cb.greaterThanOrEqualTo(root.get("height"), height);
    }

    public static Specification<Site> siteDiffGreaterOrEqualTo(Integer diff) {
        return (root, query, cb) -> {
            if(diff == 0)
                return null;
            Join<Site, Sector> sectorJoin = root.join("sectorList");
            Join<Sector, Ascent> ascentJoin = sectorJoin.join("ascentList");
            query.distinct(true);
            return cb.greaterThanOrEqualTo(ascentJoin.get("difficulty"), diff);
        };
    }

    public static Specification<Site> siteDiffLessOrEqualTo(Integer diff) {
        return (root, query, cb) -> {
            if(diff == 0)
                return null;
            Join<Site, Sector> sectorJoin = root.join("sectorList");
            Join<Sector, Ascent> ascentJoin = sectorJoin.join("ascentList");
            query.distinct(true);
            return cb.lessThanOrEqualTo(ascentJoin.get("difficulty"), diff);
        };
    }

    public static Specification<Site> siteIsOfficial(String official) {
        return (root, query, cb) -> official.equals("") ? null : cb.equal(root.get("official"), Boolean.valueOf(official));
    }
}