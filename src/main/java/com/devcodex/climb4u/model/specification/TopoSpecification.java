package com.devcodex.climb4u.model.specification;

import com.devcodex.climb4u.model.Topo;
import org.springframework.data.jpa.domain.Specification;

public class TopoSpecification {

    public static Specification<Topo> topoTitleContains(String title) {
        return (root, query, cb) -> title == null ? null : cb.like(root.get("title"), "%" + title + "%");
    }

    public static Specification<Topo> topoSite(Integer site) {
        return (root, query, cb) -> site == null ? null : cb.equal(root.get("site").get("id"), site);
    }

    public static Specification<Topo> topoAscentsBetween(int min, int max) {
        return (root, query, cb) -> cb.between(root.get("numAscents"), min, max);
    }

    public static Specification<Topo> topoHeightBetween(int min, int max) {
        return (root, query, cb) -> cb.between(root.get("height"), min, max);
    }

    public static Specification<Topo> topoAvailable(boolean available) {
        return (root, query, cb) -> cb.equal(root.get("available"), available);
    }

    public static Specification<Topo> topoMinDifficulty(int value) {
        return (root, query, cb) -> cb.greaterThanOrEqualTo(root.get("diffMin"), value);
    }

    public static Specification<Topo> topoMaxDifficulty(int value) {
        return (root, query, cb) -> cb.lessThanOrEqualTo(root.get("diffMax"), value);
    }
}