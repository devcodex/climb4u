package com.devcodex.climb4u.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;

@Entity @Table
@Getter @Setter @ToString
public class Sector {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String ascentImage;

    @ManyToOne
    @JoinColumn(name = "site_id")
    private Site site;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sector")
    private List<Ascent> ascentList;

    public String getMinDiff() {
        if(this.ascentList.size() == 0) {
            return "n/a";
        }
        int diffMin = 9;
        char diffMinSub = 'c';

        for(Ascent ascent : this.ascentList) {
            if(diffMin > ascent.getDifficulty()) {
                diffMin = ascent.getDifficulty();
                diffMinSub = ascent.getSubDifficulty().charAt(0);
            } else if(diffMin == ascent.getDifficulty() && Character.getNumericValue(diffMinSub) > Character.getNumericValue(ascent.getSubDifficulty().charAt(0))) {
                diffMinSub = ascent.getSubDifficulty().charAt(0);
            }
        }
        return "" + diffMin + diffMinSub;
    }

    public String getMaxDiff() {
        if(this.ascentList.size() == 0) {
            return "n/a";
        }
        int diffMax = 0;
        char diffMaxSub = 'a';

        for(Ascent ascent : this.ascentList) {
            if(diffMax < ascent.getDifficulty()) {
                diffMax = ascent.getDifficulty();
                diffMaxSub = ascent.getSubDifficulty().charAt(0);
            } else if(diffMax == ascent.getDifficulty() && Character.getNumericValue(diffMaxSub) > Character.getNumericValue(ascent.getSubDifficulty().charAt(0))) {
                diffMaxSub = ascent.getSubDifficulty().charAt(0);
            }
        }
        return "" + diffMax + diffMaxSub;
    }
}