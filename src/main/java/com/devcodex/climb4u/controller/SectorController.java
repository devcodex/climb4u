package com.devcodex.climb4u.controller;

import com.devcodex.climb4u.model.Ascent;
import com.devcodex.climb4u.model.Sector;
import com.devcodex.climb4u.service.SectorService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/sites/sectors")
public class SectorController {
    @Autowired
    SectorService sectorService;

    @GetMapping("/{sectorId}")
    public String getSector(@PathVariable("sectorId") int sectorId, Model model) {
        try {
            Sector sector = sectorService.find(sectorId);
            model.addAttribute("sector", sector);
            model.addAttribute("newAscent", new Ascent());
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return "sites/sector/singleSector";
    }
}
