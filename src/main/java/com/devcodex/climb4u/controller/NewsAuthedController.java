package com.devcodex.climb4u.controller;

import com.devcodex.climb4u.model.News;
import com.devcodex.climb4u.model.UserAccount;
import com.devcodex.climb4u.service.NewsService;
import com.devcodex.climb4u.service.UserAccountService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Controller for all News related views that require the user to be authenticated
 */
@Controller
@RequestMapping("/user/news")
public class NewsAuthedController {
    @Autowired
    private UserAccountService userAccountService;
    @Autowired
    private NewsService newsService;

    @GetMapping("/new")
    public String getNewSite(Model model, HttpServletRequest req) {
        if(req.isUserInRole("ROLE_ADMIN") || req.isUserInRole("ROLE_OFFICIAL")) {
            model.addAttribute("newNews", new News());
        } else {
            model.addAttribute("statusCode", 403);
            return "error";
        }
        return "news/newNews";
    }

    @PostMapping("/new")
    public String postNewNews(@ModelAttribute("newNews") @Valid News news, Principal principal, Model model, RedirectAttributes redirectAttributes, HttpServletRequest req) {
        if(req.isUserInRole("ROLE_ADMIN") || req.isUserInRole("ROLE_OFFICIAL")) {
            UserAccount createdBy = this.userAccountService.findByUsername(principal.getName());
            News newNews = new News();
            newNews.setTitle(news.getTitle());
            newNews.setContent(news.getContent());
            newNews.setCreatedBy(createdBy);
            newNews.setCreationDate(new Timestamp(new Date().getTime()));
            News created = this.newsService.save(newNews);
            redirectAttributes.addFlashAttribute("created", created.getTitle());
            return "redirect:/news/";
        } else {
            model.addAttribute("statusCode", 403);
            return "error";
        }
    }

    @GetMapping("modifyNews/{newsId}")
    public String getModifySite(@PathVariable("newsId") int newsId, Model model, Principal principal) {
        try {
            News currentNews = newsService.find(newsId);
            if(!checkOwnership(principal, currentNews)) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            model.addAttribute("modifyNews", currentNews);
        } catch (NotFoundException e) {
            System.out.println("Could not find news with id: " + newsId);
            e.printStackTrace();
        }
        return "news/editNews";
    }

    @PostMapping("modifyNews/{newsId}")
    public String postModifySite(@PathVariable("newsId") int newsId, @ModelAttribute("modifyNews") News modifiedNews, RedirectAttributes redirectAttributes, Model model, Principal principal){
        try {
            News currentNews = newsService.find(newsId);
            if(!checkOwnership(principal, currentNews) && modifiedNews == null) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            currentNews.setTitle(modifiedNews.getTitle());
            currentNews.setContent(modifiedNews.getContent());
            this.newsService.update(currentNews);
            redirectAttributes.addFlashAttribute("modified", currentNews.getTitle());
        } catch (NotFoundException e) {
            System.out.println("Could not find news with id: " + newsId);
            e.printStackTrace();
        }
        return "redirect:/news/";
    }

    @GetMapping("deleteNews/{newsId}")
    public String getDeleteSite(@PathVariable("newsId") int newsId, RedirectAttributes redirectAttributes, Model model, Principal principal) {
        try {
            News currentNews = newsService.find(newsId);
            if (!checkOwnership(principal, currentNews)) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            newsService.delete(currentNews.getId());
            redirectAttributes.addFlashAttribute("deletedNews", currentNews.getTitle());
        } catch (NotFoundException e) {
            System.out.println("Could not find news with id: " + newsId);
            e.printStackTrace();
            return "/sites/";
        }
        return "redirect:/news/";
    }

    // Checks if current user is the creator of the site
    private boolean checkOwnership(Principal principal, News news) {
        UserAccount user = this.userAccountService.findByUsername(principal.getName());
        return user.getId() == news.getCreatedBy().getId();
    }
}
