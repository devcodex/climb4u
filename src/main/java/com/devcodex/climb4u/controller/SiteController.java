package com.devcodex.climb4u.controller;

import com.devcodex.climb4u.model.Sector;
import com.devcodex.climb4u.model.Site;
import com.devcodex.climb4u.model.SiteComment;
import com.devcodex.climb4u.model.dto.SiteDTO;
import com.devcodex.climb4u.model.dto.TopoDTO;
import com.devcodex.climb4u.service.DepartmentService;
import com.devcodex.climb4u.service.SectorService;
import com.devcodex.climb4u.service.SiteCommentService;
import com.devcodex.climb4u.service.SiteService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;

/**
 * Controller for all Site related views that do not require the user to be authenticated
 */
@Controller
@RequestMapping("/sites")
public class SiteController {
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private SiteService siteService;
    @Autowired
    private SiteCommentService siteCommentService;
    @Autowired
    private SectorService sectorService;

    /**
     * Renders main site view, by default it contains the full list of site, and it allows filtering of said list
     * @param pageNum page number for displaying paged site list
     * @param siteDTO object containing information to filter the site search
     * @param model data sent to template
     * @return list of available sites, if user changed parameters, it filters the list
     */
    @RequestMapping("/")
    public String getSites(@RequestParam(value = "page", required = false) Integer pageNum, @ModelAttribute("siteSearch") SiteDTO siteDTO, Model model) {
        if(!siteDTO.isInit()) {
            siteDTO.setInit(true);
        }
        model.addAttribute("departmentList", departmentService.findAll());
        model.addAttribute("siteList", siteService.findAllByAvailable(true));
        ArrayList<Site> queryResult = this.siteService.findAllByCriteria(siteDTO);
        model.addAttribute("queryResult", queryResult);
        model.addAttribute("page", pageNum);
        model.addAttribute("siteCount", queryResult.size());
        return "sites/sites";
    }

    /**
     * Renders the view for a single site
     * @param siteId Id of site to be displayed
     * @param model data sent back to view
     * @return rendered view of selected site
     */
    @GetMapping("/{id}")
    public String getSingleSite(@PathVariable("id") int siteId, Model model) {
        try {
            Site site = this.siteService.find(siteId);
            ArrayList<Sector> sectorList = this.sectorService.findAllBySite(site);
            ArrayList<SiteComment> commentList = siteCommentService.findAllBySite(site);
            if(site.getSectorList().size() > 0) {
                model.addAttribute("sectorList", sectorList);
            }

            model.addAttribute("site", site);
            model.addAttribute("commentList", commentList);
            model.addAttribute("newComment", new SiteComment());
        } catch (NotFoundException e) {
            model.addAttribute("statusCode", 404);
            return "error";
        }
        return "sites/singleSite";
    }

    @GetMapping("/{id}/topoRedirect")
    public String getTopoSearchBySite(@PathVariable("id") int siteId, RedirectAttributes redirectAttributes) {
        TopoDTO searchTopo = new TopoDTO();
        searchTopo.setSite(siteId);
        redirectAttributes.addFlashAttribute("topoSearch", searchTopo);
        return "redirect:/topos/";
    }
}
