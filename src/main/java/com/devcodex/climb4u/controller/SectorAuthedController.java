package com.devcodex.climb4u.controller;

import com.devcodex.climb4u.model.Ascent;
import com.devcodex.climb4u.model.Sector;
import com.devcodex.climb4u.model.Site;
import com.devcodex.climb4u.model.UserAccount;
import com.devcodex.climb4u.service.AscentService;
import com.devcodex.climb4u.service.SectorService;
import com.devcodex.climb4u.service.SiteService;
import com.devcodex.climb4u.service.UserAccountService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;

@Controller
@RequestMapping("/user/sites/sectors")
public class SectorAuthedController {
    @Autowired
    private SiteService siteService;
    @Autowired
    private UserAccountService userAccountService;
    @Autowired
    private SectorService sectorService;
    @Autowired
    private AscentService ascentService;

    /**
     * Renders the view to allow the creation of a new sector associated to a site
     * @param model data sent to template
     * @return the view to allow the creation of a new sector
     */
    @GetMapping("/new/{siteId}")
    public String getNewSite(@PathVariable("siteId") int siteId, Model model, Principal principal) {
        Site currentSite;
        try {
            currentSite = siteService.find(siteId);
            UserAccount currentUser = userAccountService.findByUsername(principal.getName());
            if(currentUser.getId() != currentSite.getCreatedBy().getId()) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
        } catch (NotFoundException e) {
            e.printStackTrace();
            return "redirect:/sites/";
        }
        model.addAttribute("currentSite", currentSite);
        return "sites/sector/newSector";
    }

    @PostMapping("/new/{siteId}")
    public String postNewSite(@PathVariable("siteId") int siteId, @ModelAttribute("name") String name, @ModelAttribute("ascentImage") String ascentImage, Model model) {
        Site currentSite;
        try {
            currentSite = siteService.find(siteId);
        } catch (NotFoundException e) {
            e.printStackTrace();
            return "redirect:/sites/";
        }

        Sector sector = new Sector();
        sector.setName(name);
        sector.setAscentImage(ascentImage);
        sector.setSite(currentSite);
        sectorService.save(sector);
        return "redirect:/sites/" + siteId;
    }

    @PostMapping("/{sectorId}/newAscent")
    public String postNewAscent(@PathVariable("sectorId") int sectorId, @ModelAttribute("ascent") Ascent ascent, Model model, RedirectAttributes redirectAttributes, Principal principal) {
        try {
            UserAccount userAccount = userAccountService.findByUsername(principal.getName());
            Sector sector = sectorService.find(sectorId);
            if(userAccount.getId() != sector.getSite().getCreatedBy().getId()) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            ascent.setSector(sector);
            ascentService.save(ascent);
            redirectAttributes.addFlashAttribute("created", ascent.getName());
        } catch (NotFoundException e) {
            e.printStackTrace();
            return "redirect:/sites/";
        }
        return "redirect:/sites/sectors/" + sectorId;
    }

    @GetMapping("/modify/{id}")
    public String getModifySector(@PathVariable("id") int sectorId, RedirectAttributes redirectAttributes, Model model, Principal principal) {
        Sector sector;
        try {
            UserAccount userAccount = userAccountService.findByUsername(principal.getName());
            sector = sectorService.find(sectorId);
            if(userAccount.getId() != sector.getSite().getCreatedBy().getId()) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            model.addAttribute("modifySector", sector);
        } catch (NotFoundException e) {
            System.out.println("Could not find sector with id: " + sectorId);
            e.printStackTrace();
            return "/sites/";
        }
        return "sites/sector/editSector";
    }

    @PostMapping("/modify/{id}")
    public String postModifySector(@PathVariable("id") int sectorId, @ModelAttribute("modifySector") Sector modifySector, Model model, Principal principal, RedirectAttributes redirectAttributes) {
        Sector sector;
        try {
            UserAccount userAccount = userAccountService.findByUsername(principal.getName());
            sector = sectorService.find(sectorId);
            if(userAccount.getId() != sector.getSite().getCreatedBy().getId()) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            sector.setName(modifySector.getName());
            sector.setAscentImage(modifySector.getAscentImage());
            this.sectorService.update(sector);
            redirectAttributes.addFlashAttribute("modifiedSector", sector.getName());
        } catch (NotFoundException e) {
            System.out.println("Could not find sector with id: " + sectorId);
            e.printStackTrace();
            return "redirect:/sites/";
        }
        return "redirect:/sites/" + sector.getSite().getId();
    }

    @GetMapping("/ascent/modify/{id}")
    public String getModifyAscent(@PathVariable("id") int ascentId, RedirectAttributes redirectAttributes, Model model, Principal principal) {
        Ascent ascent;
        try {
            UserAccount userAccount = userAccountService.findByUsername(principal.getName());
            ascent = ascentService.find(ascentId);
            if(userAccount.getId() != ascent.getSector().getSite().getCreatedBy().getId()) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            model.addAttribute("modifyAscent", ascent);
        } catch (NotFoundException e) {
            System.out.println("Could not find ascent with id: " + ascentId);
            e.printStackTrace();
            return "/sites/";
        }
        return "sites/sector/editAscent";
    }

    @PostMapping("/ascent/modify/{id}")
    public String postModifySector(@PathVariable("id") int ascentId, @ModelAttribute("modifyAscent") Ascent modifyAscent, Model model, Principal principal, RedirectAttributes redirectAttributes) {
        Ascent ascent;
        try {
            UserAccount userAccount = userAccountService.findByUsername(principal.getName());
            ascent = ascentService.find(ascentId);
            if(userAccount.getId() != ascent.getSector().getSite().getCreatedBy().getId()) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            ascent.setNumeration(modifyAscent.getNumeration());
            ascent.setName(modifyAscent.getName());
            ascent.setHeight(modifyAscent.getHeight());
            ascent.setDifficulty(modifyAscent.getDifficulty());
            ascent.setSubDifficulty(modifyAscent.getSubDifficulty());
            this.ascentService.update(ascent);
            redirectAttributes.addFlashAttribute("modifiedAscent", ascent.getName());
        } catch (NotFoundException e) {
            System.out.println("Could not find sector with id: " + ascentId);
            e.printStackTrace();
            return "redirect:/sites/";
        }
        return "redirect:/sites/sectors/" + ascent.getSector().getId();
    }

    @GetMapping("/delete/{id}")
    public String getDeleteSector(@PathVariable("id") int sectorId, RedirectAttributes redirectAttributes, Model model, Principal principal) {
        Sector sector;
        try {
            UserAccount userAccount = userAccountService.findByUsername(principal.getName());
            sector = sectorService.find(sectorId);
            if(userAccount.getId() != sector.getSite().getCreatedBy().getId()) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            sectorService.delete(sector.getId());
            redirectAttributes.addFlashAttribute("deletedSector", sector.getName());
        } catch (NotFoundException e) {
            System.out.println("Could not find sector with id: " + sectorId);
            e.printStackTrace();
            return "redirect:/sites/";
        }
        return "redirect:/sites/" + sector.getSite().getId();
    }

    @GetMapping("/ascent/delete/{id}")
    public String getDeleteAscent(@PathVariable("id") int ascentId, RedirectAttributes redirectAttributes, Model model, Principal principal) {
        Ascent ascent;
        try {
            UserAccount userAccount = userAccountService.findByUsername(principal.getName());
            ascent = ascentService.find(ascentId);
            if(userAccount.getId() != ascent.getSector().getSite().getCreatedBy().getId()) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            ascentService.delete(ascent.getId());
            redirectAttributes.addFlashAttribute("deletedSector", ascent.getName());
        } catch (NotFoundException e) {
            System.out.println("Could not find ascent with id: " + ascentId);
            e.printStackTrace();
            return "redirect:/sites/";
        }
        return "redirect:/sites/sectors/" + ascent.getSector().getId();
    }
}
