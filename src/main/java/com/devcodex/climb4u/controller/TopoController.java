package com.devcodex.climb4u.controller;

import com.devcodex.climb4u.model.*;
import com.devcodex.climb4u.model.dto.TopoDTO;
import com.devcodex.climb4u.model.dto.TopoReviewDTO;
import com.devcodex.climb4u.service.*;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for all Topo related views that do not require the user to be authenticated
 */
@Controller
@RequestMapping("/topos")
public class TopoController {
    @Autowired
    TopoService topoService;
    @Autowired
    TopoLendingService topoLendingService;
    @Autowired
    TopoReviewService topoReviewService;
    @Autowired
    SiteService siteService;
    @Autowired
    UserAccountService userAccountService;

    /**
     * Renders the view containing the list of topos, filters said list is user changes data on topoDTO
     * @param pageNum page number for displaying paged topo list
     * @param topoDTO object containing data to filter the list of topos
     * @param model data sent to the view
     * @return rendered view with the list of topos
     */
    @RequestMapping("/")
    public String getTopos(@RequestParam(value = "page", required = false) Integer pageNum, @ModelAttribute("topoSearch") TopoDTO topoDTO, Model model) {
        if(!topoDTO.isInit()) {
            topoDTO.setInit(true);
        }
        ArrayList<Topo> queryResult = this.topoService.findAllByCriteria(topoDTO);
        List<Site> siteList = this.siteService.findAllByAvailable(true);
        model.addAttribute("queryResult", queryResult);
        model.addAttribute("page", pageNum);
        model.addAttribute("formRequest", topoDTO.toString());
        model.addAttribute("topoCount", queryResult.size());
        model.addAttribute("siteList", siteList);
        return "topos/topos";
    }

    /**
     * Renders the view for a single Topo
     * @param topoId Id of selected topo to be viewed
     * @param topoReview object containing data in case user wants to create a new review for selected Topo
     * @param model data to be sent back to the view
     * @return rendered view with the selected Topo
     */
    @GetMapping("/{id}")
    public String getSingleTopo(@PathVariable("id") int topoId, @ModelAttribute("newReview") TopoReviewDTO topoReview, Model model, Principal principal){
        UserAccount currentUser = null;
        try {
            if(principal != null) {
                currentUser = this.userAccountService.findByUsername(principal.getName());
            }
            Topo topo = this.topoService.find(topoId);
            model.addAttribute("topo", topo);
            if(currentUser != null) {
                ArrayList<TopoLending> lendings = topoLendingService.findAllbyTopo(topo);
                ArrayList<TopoLending> userRequests = topoLendingService.findAllByTakerAndTopo(currentUser,topo);
                TopoReview userReview = topoReviewService.findUserReview(topo, currentUser);
                if(lendings != null) {
                    model.addAttribute("lendings", lendings);
                }
                if(userRequests != null) {
                    model.addAttribute("userRequests", userRequests);
                }
                if(userReview != null) {
                    model.addAttribute("userReview", userReview);
                }
            }
            model.addAttribute("listReview", this.topoReviewService.findAllbyTopo(topo));
        } catch (NotFoundException e) {
            model.addAttribute("statusCode", 404);
            return "error";
        }
        return "topos/singleTopo";
    }
}
