package com.devcodex.climb4u.controller;

import com.devcodex.climb4u.model.News;
import com.devcodex.climb4u.model.UserAccount;
import com.devcodex.climb4u.model.dto.UserAccountDTO;
import com.devcodex.climb4u.service.NewsService;
import com.devcodex.climb4u.service.SiteService;
import com.devcodex.climb4u.service.TopoService;
import com.devcodex.climb4u.service.UserAccountService;
import com.devcodex.climb4u.service.impl.UserAccountServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;

@Controller
@RequestMapping("/")
public class DefaultController {
    @Autowired
    private UserAccountService userAccountService;
    @Autowired
    private NewsService newsService;
    @Autowired
    private TopoService topoService;
    @Autowired
    private SiteService siteService;

    /**
     * Renders index page
     * @return view of index page
     */
    @GetMapping("/")
    public String getIndex(Model model){
        News latestNews = newsService.findLatestNews();
        model.addAttribute("topoCount", topoService.countAllByAvailable(true));
        model.addAttribute("siteCount", siteService.countAll());
        if(latestNews != null) {
            model.addAttribute("latestNews", latestNews);
        }
        return "index";
    }

    /**
     * Renders view that allows the user to login
     * @return view containing login form
     */
    @GetMapping("/login")
    public String getLogin() {
        return "login";
    }

    /**
     * Renders view that allows a user to register for a new account
     * @param model data to be sent back to view
     * @return view containing registration form
     */
    @GetMapping("/register")
    public String getRegister(Model model) {
        model.addAttribute("user", new UserAccountDTO());
        return "register";
    }

    /**
     *
     * @param userAccountDTO object containing the information for new user
     * @param result validation object
     * @return view containing confirmation of user creation
     */
    @PostMapping("/register")
    public ModelAndView postRegister(@ModelAttribute("user") @Valid UserAccountDTO userAccountDTO,
                                     BindingResult result) {
        UserAccount registered = null;
        if (result.hasErrors()
                || this.userAccountService.emailExists(userAccountDTO.getEmail())
                || this.userAccountService.usernameExists(userAccountDTO.getUsername())) {

            if(!userAccountDTO.getEmail().equals(userAccountDTO.getEmailConfirmation())) {
                result.rejectValue("emailConfirmation", "message.emailMismatch");
            }
            if(!userAccountDTO.getPassword().equals(userAccountDTO.getPasswordConfirmation())) {
                result.rejectValue("passwordConfirmation", "message.passwordMismatch");
            }
            if(this.userAccountService.emailExists(userAccountDTO.getEmail())) {
                result.rejectValue("email", "message.emailExists");
            }
            if(this.userAccountService.usernameExists(userAccountDTO.getUsername())) {
                result.rejectValue("username", "message.usernameExists");
            }
            return new ModelAndView("register", "user", userAccountDTO);
        }
        else {
            registered = createUserAccount(userAccountDTO, result);
            return new ModelAndView("registerSuccess", "user", registered);
        }
    }

    //Registers the user
    private UserAccount createUserAccount(UserAccountDTO accountDto, BindingResult result) {
        UserAccount registered;
        try {
            registered = this.userAccountService.registerNewUser(accountDto);
        } catch (UserAccountServiceImpl.EmailExistsException e) {
            return null;
        }
        return registered;
    }
}