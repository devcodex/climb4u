package com.devcodex.climb4u.controller;

import com.devcodex.climb4u.model.Site;
import com.devcodex.climb4u.model.SiteComment;
import com.devcodex.climb4u.model.UserAccount;
import com.devcodex.climb4u.model.dto.SiteDTO;
import com.devcodex.climb4u.service.DepartmentService;
import com.devcodex.climb4u.service.SiteCommentService;
import com.devcodex.climb4u.service.SiteService;
import com.devcodex.climb4u.service.UserAccountService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Controller for all Site related views that require the user to be authenticated
 */
@Controller
@RequestMapping("/user/sites")
public class SiteAuthedController {
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private SiteService siteService;
    @Autowired
    private UserAccountService userAccountService;
    @Autowired
    private SiteCommentService siteCommentService;

    /**
     * Renders the view to allow the creation of a new site
     * @param model data sent to template
     * @return the view to allow the creation of a new site
     */
    @GetMapping("/new")
    public String getNewSite(Model model) {
        model.addAttribute("newSite", new SiteDTO());
        model.addAttribute("departmentList", departmentService.findAll());
        return "sites/newSite";
    }

    /**
     * Creates a new site
     * @param site object containing the information to create a new site
     * @param principal logged user info
     * @param model data sent to template in case of error
     * @param redirectAttributes contains information passed to the template post creation of new site.
     * @return
     */
    @PostMapping("/new")
    public String postNewSite(@ModelAttribute("newSite") @Valid SiteDTO site, Principal principal, Model model, RedirectAttributes redirectAttributes) {
        UserAccount createdBy = this.userAccountService.findByUsername(principal.getName());
        Site newSite = new Site();
        newSite.setName(site.getName());
        try {
            newSite.setDepartment(this.departmentService.find(site.getDepartmentId()));
        } catch (NotFoundException e) {
            model.addAttribute("error", "department not found");
            System.out.println("Selected department not found");
            e.printStackTrace();
            return "sites/newSite";
        }
        newSite.setType(site.getType());
        newSite.setHeight(site.getHeight());
        newSite.setCoverImage(site.getCoverImage());
        newSite.setLocation(site.getLocation());
        newSite.setDescription(site.getDescription());
        newSite.setAccessNotes(site.getAccessNotes());
        newSite.setAvailable(false);
        newSite.setCreatedBy(createdBy);
        newSite.setOfficial(false);
        Site created = this.siteService.save(newSite);
        redirectAttributes.addFlashAttribute("created", created.getName());
        return "redirect:/sites/" + created.getId();
    }

    /**
     * Toggles the visibility of a site
     * @param siteId Id of site selected to be toggled.
     * @param redirectAttributes contains information passed to the template post visibility change.
     * @param model data sent to template in case of error.
     * @param principal logged user info.
     * @return the view of the affected site with a success message
     */
    @GetMapping("toggleVisible/{siteId}")
    public String getToogleAvailableSite(@PathVariable("siteId") int siteId, RedirectAttributes redirectAttributes, Principal principal, Model model) {
        try {
            Site currentSite = siteService.find(siteId);
            if(!checkOwnership(principal, currentSite)) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            currentSite.setAvailable(!currentSite.isAvailable());
            siteService.update(currentSite);
            redirectAttributes.addFlashAttribute("visibility", currentSite.getName());
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return "redirect:/sites/" + siteId;
    }

    @GetMapping("toggleOfficial/{siteId}")
    public String getToogleOfficialSite(@PathVariable("siteId") int siteId, RedirectAttributes redirectAttributes, Model model, HttpServletRequest req) {
        try {
            Site currentSite = siteService.find(siteId);
            if(!req.isUserInRole("ROLE_ADMIN") || !req.isUserInRole("ROLE_ADMIN")) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            currentSite.setOfficial(!currentSite.isOfficial());
            siteService.update(currentSite);
            redirectAttributes.addFlashAttribute("official", true);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return "redirect:/sites/" + siteId;
    }

    /**
     * Renders the form to allow the modification of a selected site
     * @param siteId Id of site selected for modification.
     * @param model data sent to template.
     * @param principal logged user info.
     * @return the template with the loaded site information for modification
     */
    @GetMapping("modifySite/{siteId}")
    public String getModifySite(@PathVariable("siteId") int siteId, Model model, Principal principal) {
        try {
            Site currentSite = siteService.find(siteId);
            if(!checkOwnership(principal, currentSite)) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            SiteDTO modifySite = new SiteDTO(currentSite);
            model.addAttribute("departmentList", departmentService.findAll());
            model.addAttribute("modifySite", modifySite);
        } catch (NotFoundException e) {
            System.out.println("Could not find topo with id: " + siteId);
            e.printStackTrace();
        }
        return "sites/editSite";
    }

    /**
     * Modifies a selected Site
     * @param siteId Id of the site to be modified
     * @param modifiedSite object containing the modified data to be added to the original Site object
     * @param redirectAttributes contains information passed to the template post modifications.
     * @param model data to be sent to the view
     * @param principal logged user info
     * @return the view of the affected site with a success message
     */
    @PostMapping("modifySite/{siteId}")
    public String postModifySite(@PathVariable("siteId") int siteId, @ModelAttribute("modifySite") SiteDTO modifiedSite, RedirectAttributes redirectAttributes, Model model, Principal principal){
        try {
            Site site = siteService.find(siteId);
            if(!checkOwnership(principal, site) && modifiedSite == null) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            site.setName(modifiedSite.getName());
            site.setDepartment(departmentService.find(modifiedSite.getDepartmentId()));
            site.setType(modifiedSite.getType());
            site.setHeight(modifiedSite.getHeight());
            site.setCoverImage(modifiedSite.getCoverImage());
            site.setLocation(modifiedSite.getLocation());
            site.setDescription(modifiedSite.getDescription());
            site.setAccessNotes(modifiedSite.getAccessNotes());
            this.siteService.update(site);
            redirectAttributes.addFlashAttribute("modified", site.getName());
        } catch (NotFoundException e) {
            System.out.println("Could not find topo with id: " + siteId);
            e.printStackTrace();
        }
        return "redirect:/sites/" + siteId;
    }

    /**
     * Route used to delete a record contained in site table
     * @param siteId Id of site selected for deletion.
     * @param redirectAttributes contains information passed to the template post deletion.
     * @param model data sent to template.
     * @param principal logged user info.
     * @return to Sites main page, contains model information
     */
    @GetMapping("deleteSite/{siteId}")
    public String getDeleteSite(@PathVariable("siteId") int siteId, RedirectAttributes redirectAttributes, Model model, Principal principal) {
        try {
            Site currentSite = siteService.find(siteId);
            if (!checkOwnership(principal, currentSite)) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            siteService.delete(currentSite.getId());
            redirectAttributes.addFlashAttribute("deletedSite", currentSite.getName());
        } catch (NotFoundException e) {
            System.out.println("Could not find topo with id: " + siteId);
            e.printStackTrace();
            return "redirect:/sites/";
        }
        return "redirect:/sites/";
    }

    @PostMapping("newComment/{siteId}")
    public String postNewComment(@PathVariable("siteId") int siteId, @ModelAttribute("newComment") SiteComment newComment,
                                 RedirectAttributes redirectAttributes, Model model, Principal principal) {
        Site site;
        try {
            UserAccount user = this.userAccountService.findByUsername(principal.getName());
            newComment.setCreationDate(new Timestamp(new Date().getTime()));
            newComment.setCreatedBy(user);
            newComment.setSite(siteService.find(siteId));
            siteCommentService.save(newComment);
            redirectAttributes.addFlashAttribute("addedComment", true);
        } catch (NotFoundException e) {
            e.printStackTrace();
            model.addAttribute("statusCode", 404);
            return "error";
        }
        return "redirect:/sites/" + siteId;
    }

    @GetMapping("modifyComment/{commentId}")
    public String getModifyComment(@PathVariable("commentId") int commentId, Model model) {
        SiteComment siteComment;
        try {
            siteComment = siteCommentService.find(commentId);
            model.addAttribute("modifyComment", siteComment);
        } catch (NotFoundException e) {
            e.printStackTrace();
            model.addAttribute("statusCode", 404);
            return "error";
        }
        return "sites/editComment";
    }

    @PostMapping("modifyComment/{commentId}")
    public String postModifyComment(@PathVariable("commentId") int commentId, @ModelAttribute("modifyComment") SiteComment modifyComment,
                                    Model model, RedirectAttributes redirectAttributes, Principal principal, HttpServletRequest request) {
        SiteComment siteComment;
        UserAccount user = this.userAccountService.findByUsername(principal.getName());
        try {
            siteComment = siteCommentService.find(commentId);
            if(user.getId() == siteComment.getCreatedBy().getId() || request.isUserInRole("ROLE_ADMIN") || request.isUserInRole("ROLE_OFFICIAL")) {
                siteComment.setText(modifyComment.getText());
                siteComment.setLastEditedBy(user);
                siteComment.setModification_reason(modifyComment.getModification_reason());
                siteComment.setLastModified(new Timestamp(new Date().getTime()));

                redirectAttributes.addFlashAttribute("modifiedComment", true);
            } else {
                model.addAttribute("statusCode", 403);
                return "error";
            }
        } catch (NotFoundException e) {
            e.printStackTrace();
            model.addAttribute("statusCode", 404);
            return "error";
        }
        return "redirect:/sites/" + siteComment.getSite().getId();
    }

    @GetMapping("deleteComment/{commentId}")
    public String getDeleteComment(@PathVariable("commentId") int commentId, RedirectAttributes redirectAttributes, Model model, Principal principal,
                                   HttpServletRequest request) {
        SiteComment siteComment;
        try {
            siteComment = siteCommentService.find(commentId);
            UserAccount user = userAccountService.findByUsername(principal.getName());
            if (siteComment.getCreatedBy().getId() == user.getId() || request.isUserInRole("ROLE_ADMIN") || request.isUserInRole("ROLE_OFFICIAL")) {
                siteCommentService.delete(siteComment.getId());
                redirectAttributes.addFlashAttribute("deletedComment", true);
            } else {
                model.addAttribute("statusCode", 403);
                return "error";
            }
        } catch (NotFoundException e) {
            e.printStackTrace();
            model.addAttribute("statusCode", 404);
            return "error";
        }
        return "redirect:/sites/" + siteComment.getSite().getId();
    }

    // Checks if current user is the creator of the site
    private boolean checkOwnership(Principal principal, Site site) {
        UserAccount user = this.userAccountService.findByUsername(principal.getName());
        return user.getId() == site.getCreatedBy().getId();
    }
}
