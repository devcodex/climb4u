package com.devcodex.climb4u.controller;

import com.devcodex.climb4u.model.*;
import com.devcodex.climb4u.model.dto.TopoDTO;
import com.devcodex.climb4u.model.dto.TopoReviewDTO;
import com.devcodex.climb4u.service.*;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for all Topo related views that do not require the user to be authenticated
 */
@Controller
@RequestMapping("/news")
public class NewsController {
    @Autowired
    NewsService newsService;

    @RequestMapping("/")
    public String getNews(Model model) {
        ArrayList<News> newsList = newsService.findAllByCreationDate();
        if(newsList.size() != 0) {
            model.addAttribute("newsList", newsService.findAllByCreationDate());
        }
        return "news/news";
    }
}
