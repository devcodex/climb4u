package com.devcodex.climb4u.controller;

import com.devcodex.climb4u.model.Site;
import com.devcodex.climb4u.model.Topo;
import com.devcodex.climb4u.model.TopoLending;
import com.devcodex.climb4u.model.UserAccount;
import com.devcodex.climb4u.service.SiteService;
import com.devcodex.climb4u.service.TopoLendingService;
import com.devcodex.climb4u.service.UserAccountService;
import com.devcodex.climb4u.service.impl.TopoLendingServiceImpl;
import com.devcodex.climb4u.service.impl.TopoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.ArrayList;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserAccountService userAccountService;

    @Autowired
    private TopoServiceImpl topoService;

    @Autowired
    private SiteService siteService;

    @Autowired
    private TopoLendingService topoLendingService;

    /**
     *
     * @param model data to be sent to the view
     * @return
     */
    @GetMapping("/dashboard")
    public String getIndex(Model model, Principal principal){
        UserAccount currentUser = this.userAccountService.findByUsername(principal.getName());
        ArrayList<Topo> topoList = topoService.findAllByCreatedBy(currentUser);
        ArrayList<Site> siteList = siteService.findAllByCreatedBy(currentUser);
        ArrayList<TopoLending> topoLendingList = topoLendingService.findAllByTaker(currentUser);
        if(topoList.size() != 0) {
            model.addAttribute("topoList", topoList);
        }
        if(siteList.size() != 0) {
            model.addAttribute("siteList", siteList);
        }
        if(topoLendingList.size() != 0) {
            model.addAttribute("lendingList", topoLendingList);
        }
        return "dashboard";
    }

    @GetMapping("/postLoginAuth")
    public String getPostLoginAuth(HttpSession session, Principal principal, HttpServletRequest req) {
        if(principal != null) {
            UserAccount currentUser = userAccountService.findByUsername(principal.getName());
            req.getSession().setAttribute("authUser", currentUser);
        }
        return "redirect:/";
    }
}