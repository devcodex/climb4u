package com.devcodex.climb4u.controller;

import com.devcodex.climb4u.model.*;
import com.devcodex.climb4u.model.dto.TopoDTO;
import com.devcodex.climb4u.model.dto.TopoReviewDTO;
import com.devcodex.climb4u.service.*;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Controller for all Topo related views that require the user to be authenticated
 */
@Controller
@RequestMapping("/user/topos")
public class TopoAuthedController {
    @Autowired
    private TopoService topoService;
    @Autowired
    private UserAccountService userAccountService;
    @Autowired
    private TopoReviewService topoReviewService;
    @Autowired
    private TopoLendingService topoLendingService;
    @Autowired
    private SiteService siteService;

    /**
     * Renders the view with the form to create a new Topo
     * @param model data to be sent back to view
     * @return renders view containing new topo form
     */
    @GetMapping("/new")
    public String getNewTopo(Model model) {
        model.addAttribute("newTopo", new Topo());
        model.addAttribute("siteList", siteService.findAllByAvailable(true));
        return "topos/newTopo";
    }

    /**
     * Creates a new topo
     * @param topo object containing information for the new Topo
     * @param principal logged user info
     * @param redirectAttributes object to be sent to the single topo view
     * @return default Topo view
     */
    @PostMapping("/new")
    public String postNewTopo(@ModelAttribute("newTopo") @Valid Topo topo, Principal principal, RedirectAttributes redirectAttributes, Model model) {
        boolean error = false;
        UserAccount createdBy = this.userAccountService.findByUsername(principal.getName());
        if(!topo.checkValidDifficulties()) {
            model.addAttribute("errorDifficulty", true);
            error = true;
        }
        if(!topo.checkValidReleaseDate()) {
            model.addAttribute("errorDate", true);
            error = true;
        }
        if(error) {
            topo.setSite(null);
            model.addAttribute("newTopo", topo);
            model.addAttribute("siteList", siteService.findAllByAvailable(true));
            return "topos/newTopo";
        }
        topo.setCreatedBy(createdBy);
        topo.setAvailable(true);
        Topo created = this.topoService.addNewTopo(topo);
        redirectAttributes.addFlashAttribute("created", created.getTitle());
        return "redirect:/topos/" + created.getId();
    }

    /**
     * Deletes a selected Topo
     * @param topoId Id of topo to be deleted
     * @param redirectAttributes contains information to be sent to view post deletion
     * @param model contains information to be sent to view in case of error
     * @param principal logged user info
     * @return default Topo view with success information
     */
    @GetMapping("deleteTopo/{topoId}")
    public String getDeleteTopo(@PathVariable("topoId") int topoId, RedirectAttributes redirectAttributes, Model model, Principal principal) {
        try {
            UserAccount createdBy = this.userAccountService.findByUsername(principal.getName());
            Topo topo = this.topoService.findById(topoId);
            if(createdBy.getId() != topo.getCreatedBy().getId()) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            topoService.delete(topo.getId());
            redirectAttributes.addFlashAttribute("deleted", topo.getTitle());
        } catch (NotFoundException e) {
            System.out.println("Could not find topo with id: " + topoId);
            e.printStackTrace();
            return "redirect:/topos/";
        }
        return "redirect:/topos/";
    }

    /**
     * Renders the form to allow the modification of a selected Topo
     * @param topoId Id of topo selected for modification.
     * @param model data sent to template.
     * @param principal logged user info.
     * @return the template with the loaded Topo information for modification
     */
    @GetMapping("modifyTopo/{topoId}")
    public String getModifyTopo(@PathVariable("topoId") int topoId, Model model, Principal principal) {
        try {
            UserAccount createdBy = this.userAccountService.findByUsername(principal.getName());
            Topo topo = this.topoService.findById(topoId);
            if(createdBy.getId() != topo.getCreatedBy().getId()) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            model.addAttribute("siteList", siteService.findAllByAvailable(true));
            model.addAttribute("modifyTopo", topo);
        } catch (NotFoundException e) {
            System.out.println("Could not find topo with id: " + topoId);
            e.printStackTrace();
        }
        return "topos/editTopo";
    }

    /**
     * Modifies a selected Topo
     * @param modifiedTopo object containing the modified data to be added to the original Topo object
     * @param redirectAttributes contains information passed to the template post modifications.
     * @param model data to be sent to the view
     * @param principal logged user info
     * @return the view of the affected Topo with a success message
     */
    @PostMapping("modifyTopo")
    public String postModifyTopo(@ModelAttribute("modifyTopo") Topo modifiedTopo, RedirectAttributes redirectAttributes, Model model, Principal principal){
        try {
            UserAccount createdBy = this.userAccountService.findByUsername(principal.getName());
            Topo topo = this.topoService.findById(modifiedTopo.getId());
            if(createdBy.getId() != topo.getCreatedBy().getId()) {
                model.addAttribute("statusCode", 403);
                return "error";
            }
            topo.setTitle(modifiedTopo.getTitle());
            topo.setSite(modifiedTopo.getSite());
            topo.setCoverImage(modifiedTopo.getCoverImage());
            topo.setDescription(modifiedTopo.getDescription());
            topo.setDiffMin(modifiedTopo.getDiffMin());
            topo.setDiffMinSub(modifiedTopo.getDiffMinSub());
            topo.setDiffMax(modifiedTopo.getDiffMax());
            topo.setDiffMaxSub(modifiedTopo.getDiffMaxSub());
            topo.setNumAscents(modifiedTopo.getNumAscents());
            topo.setHeight(modifiedTopo.getHeight());
            topo.setReleaseDate(modifiedTopo.getReleaseDate());
            this.topoService.update(topo);
            redirectAttributes.addFlashAttribute("modified", modifiedTopo.getTitle());
        } catch (NotFoundException e) {
            System.out.println("Could not find topo with id: " + modifiedTopo.getId());
            e.printStackTrace();
        }
        return "redirect:/topos/" + modifiedTopo.getId();
    }

    /**
     * Creates a new TopoLending record
     * @param topoId Id of topo
     * @return view of selected Topo
     */
    @GetMapping("/requestTopo/{id}")
    public String getRequestTopo(@PathVariable("id") int topoId, Principal principal) {
        try {
            UserAccount currentUser = userAccountService.findByUsername(principal.getName());
            Topo requestedTopo = topoService.findById(topoId);
            if(requestedTopo.getCreatedBy().getId() != currentUser.getId()) {
                TopoLending lending = new TopoLending();
                lending.setTopo(requestedTopo);
                lending.setTaker(currentUser);
                lending.setCreationDate(new Timestamp(new Date().getTime()));
                topoLendingService.save(lending);
            }
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        return "redirect:/topos/" + topoId;
    }

    /**
     * Modifies selected TopoLending
     * @param topoId Id of the topo containing the request
     * @param accepted information of whether the request is accepted or not
     * @param requestId TopoLending request Id
     * @return
     */
    @PostMapping("answerTopoRequest")
    public String postLendingAnswer(@ModelAttribute("topoId") int topoId, @ModelAttribute("accepted") boolean accepted, @ModelAttribute("requestId") int requestId) {
        try {
            TopoLending topoLending = topoLendingService.find(requestId);
            topoLending.setAnswered(true);
            topoLending.setAccepted(accepted);
            topoLendingService.update(topoLending);
            if(accepted) {
                topoLending.getTopo().setAvailable(false);
                topoService.update(topoLending.getTopo());
            }
            return "redirect:/topos/" + topoLending.getTopo().getId();
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return "redirect:/topos/" + topoId;
    }

    @GetMapping("deleteTopoRequest/{requestId}")
    public String getDeleteTopoRequest(@PathVariable("requestId") int requestId, Principal principal, Model model, RedirectAttributes redirectAttributes) {
        TopoLending topoLending;
        try {
            topoLending = topoLendingService.find(requestId);
            UserAccount currentUser = this.userAccountService.findByUsername(principal.getName());
            if(topoLending.getTaker().getId() == currentUser.getId()){
                topoLendingService.delete(topoLending.getId());
                redirectAttributes.addFlashAttribute("requestDeleted", true);
            }
        } catch (NotFoundException e) {
            e.printStackTrace();
            model.addAttribute("statusCode", 404);
            return "error";
        }
        return "redirect:/topos/" + topoLending.getTopo().getId();
    }

    /**
     * Toggles the availability of the Topo
     * @param topoId Id of selected Topo
     * @param redirectAttributes  contains information passed to the template post availability change.
     * @return view of the selected topo
     */
    @GetMapping("toggleVisibility/{topoId}")
    public String getToogleAvailableTopo(@PathVariable("topoId") int topoId, RedirectAttributes redirectAttributes) {
        try {
            Topo currentTopo = topoService.find(topoId);
            currentTopo.setAvailable(!currentTopo.isAvailable());
            topoService.update(currentTopo);
            redirectAttributes.addFlashAttribute("availability", currentTopo.getTitle());
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return "redirect:/topos/" + topoId;
    }

    /**
     * Creates a new review for the selected Topo
     * @param topoId Id of topo that will have the new review created
     * @param review object containing the information need to create a new review
     * @param principal logged user info
     * @return view of selected topo
     */
    @PostMapping("/newReview/{topoId}")
    public String postNewReview(@PathVariable("topoId") int topoId, @ModelAttribute("newReview") @Valid TopoReviewDTO review, Principal principal) {
        try {
            UserAccount createdBy = this.userAccountService.findByUsername(principal.getName());
            Topo topo = this.topoService.findById(topoId);
            TopoReview submitReview = new TopoReview();
            submitReview.setTitle(review.getTitle());
            submitReview.setValue(review.getValue());
            submitReview.setDetails(review.getDetails());
            submitReview.setCreatedBy(createdBy);
            submitReview.setTopo(topo);
            topoReviewService.save(submitReview);
        } catch (NotFoundException e) {
            System.out.println("topo not found");
            e.printStackTrace();
            return "error";
        }
        return "redirect:/topos/" + topoId;
    }

    @GetMapping("/modifyReview/{reviewId}")
    public String getModifyReview(@PathVariable("reviewId") int reviewId, Model model, Principal principal) {
        try {
            TopoReview userReview = topoReviewService.find(reviewId);
            if(checkReviewOwnership(principal, userReview)) {
                model.addAttribute("modifyReview", userReview);
            }
        } catch (NotFoundException e) {
            model.addAttribute("statusCode", 404);
            return "error";
        }
        return "topos/reviews/editReview";
    }

    @PostMapping("/modifyReview/{reviewId}")
    public String postModifyReview(@PathVariable("reviewId") int reviewId, @ModelAttribute("modifyReview") TopoReview modifiedReview,
                                   Model model, Principal principal, RedirectAttributes redirectAttributes) {
        TopoReview userReview;
        try {
            userReview = topoReviewService.find(reviewId);
            if(checkReviewOwnership(principal, userReview)) {
                userReview.setTitle(modifiedReview.getTitle());
                userReview.setValue(modifiedReview.getValue());
                userReview.setDetails(modifiedReview.getDetails());
                topoReviewService.update(userReview);
                redirectAttributes.addFlashAttribute("modifiedReview", true);
            }
        } catch (NotFoundException e) {
            model.addAttribute("statusCode", 404);
            return "error";
        }
        return "redirect:/topos/" + userReview.getTopo().getId();
    }

    @GetMapping("/deleteReview/{reviewId}")
    public String postModifyReview(@PathVariable("reviewId") int reviewId, Model model, Principal principal, RedirectAttributes redirectAttributes) {
        TopoReview userReview;
        try {
            userReview = topoReviewService.find(reviewId);
            if(checkReviewOwnership(principal, userReview)) {
                topoReviewService.delete(userReview.getId());
                redirectAttributes.addFlashAttribute("deletedReview", true);
            }
        } catch (NotFoundException e) {
            model.addAttribute("statusCode", 404);
            return "error";
        }
            return "redirect:/topos/" + userReview.getTopo().getId();
    }

    private boolean checkReviewOwnership(Principal principal, TopoReview topoReview) {
        UserAccount user = this.userAccountService.findByUsername(principal.getName());
        return user.getId() == topoReview.getCreatedBy().getId();
    }
}
