package com.devcodex.climb4u.service;

import com.devcodex.climb4u.model.UserAccount;
import com.devcodex.climb4u.model.dto.UserAccountDTO;
import com.devcodex.climb4u.service.impl.UserAccountServiceImpl;

public interface UserAccountService extends GenericService<UserAccount> {
    UserAccount findByEmail(String email);
    UserAccount findByUsername(String username);

    boolean emailExists(String email);
    boolean usernameExists(String username);
    UserAccount registerNewUser(UserAccountDTO userAccountDTO) throws UserAccountServiceImpl.EmailExistsException;
}
