package com.devcodex.climb4u.service;

import com.devcodex.climb4u.model.Department;

public interface DepartmentService extends GenericService<Department> {
}
