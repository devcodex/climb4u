package com.devcodex.climb4u.service;

import com.devcodex.climb4u.model.Role;

public interface RoleService extends GenericService<Role> {
    Role findByName(String name);
}
