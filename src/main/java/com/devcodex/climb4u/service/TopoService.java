package com.devcodex.climb4u.service;

import com.devcodex.climb4u.model.Topo;
import com.devcodex.climb4u.model.dto.TopoDTO;
import com.devcodex.climb4u.model.UserAccount;
import javassist.NotFoundException;

import java.util.ArrayList;

public interface TopoService extends GenericService<Topo> {
    Topo findByTitle(String title);
    Topo findById(int id) throws NotFoundException;
    Long countAllByAvailable(boolean available);
    ArrayList<Topo> findAllByAvailable(boolean available);
    ArrayList<Topo> findAllByCreatedBy(UserAccount userAccount);
    ArrayList<Topo> findAllByCriteria(TopoDTO topoDTO);

    Topo addNewTopo(Topo topo);
}
