package com.devcodex.climb4u.service;

import com.devcodex.climb4u.model.Ascent;

public interface AscentService extends GenericService<Ascent> {
}
