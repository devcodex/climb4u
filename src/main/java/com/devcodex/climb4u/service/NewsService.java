package com.devcodex.climb4u.service;

import com.devcodex.climb4u.model.News;

import java.util.ArrayList;

public interface NewsService extends GenericService<News> {
    ArrayList<News> findAllByCreationDate();
    News findLatestNews();
}
