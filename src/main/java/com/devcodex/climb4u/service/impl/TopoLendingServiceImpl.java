package com.devcodex.climb4u.service.impl;

import com.devcodex.climb4u.dao.TopoLendingDao;
import com.devcodex.climb4u.model.Topo;
import com.devcodex.climb4u.model.TopoLending;
import com.devcodex.climb4u.model.UserAccount;
import com.devcodex.climb4u.service.TopoLendingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class TopoLendingServiceImpl extends GenericServiceImpl<TopoLending> implements TopoLendingService {
    @Autowired
    TopoLendingDao topoLendingDao;

    @Override
    public JpaRepository<TopoLending, Integer> getDao() {
        return topoLendingDao;
    }

    public ArrayList<TopoLending> findAllbyTopo(Topo topo) {
        return topoLendingDao.findAllByTopo(topo);
    }

    @Override
    public ArrayList<TopoLending> findAllByTakerAndTopo(UserAccount userAccount, Topo topo) {
        return topoLendingDao.findAllByTakerAndTopo(userAccount, topo);
    }

    @Override
    public ArrayList<TopoLending> findAllByTaker(UserAccount userAccount) {
        return topoLendingDao.findAllByTaker(userAccount);
    }
}
