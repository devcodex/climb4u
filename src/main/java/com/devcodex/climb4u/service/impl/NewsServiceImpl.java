package com.devcodex.climb4u.service.impl;


import com.devcodex.climb4u.dao.NewsDao;
import com.devcodex.climb4u.model.News;
import com.devcodex.climb4u.service.NewsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class NewsServiceImpl extends GenericServiceImpl<News> implements NewsService {

    private final NewsDao newsDao;

    @Override
    public JpaRepository<News, Integer> getDao() {
        return newsDao;
    }

    @Override
    public ArrayList<News> findAllByCreationDate() {
        return newsDao.findAllByOrderByCreationDateDesc();
    }

    @Override
    public News findLatestNews() {
        ArrayList<News> newsList = newsDao.findAllByOrderByCreationDateDesc();
        if(newsList.size() > 0) {
            return newsList.get(0);
        } else {
            return null;
        }
    }
}
