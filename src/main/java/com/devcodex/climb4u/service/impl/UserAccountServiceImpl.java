package com.devcodex.climb4u.service.impl;

import com.devcodex.climb4u.dao.RoleDao;
import com.devcodex.climb4u.dao.UserAccountDao;
import com.devcodex.climb4u.model.UserAccount;
import com.devcodex.climb4u.model.dto.UserAccountDTO;
import com.devcodex.climb4u.service.UserAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserAccountServiceImpl extends GenericServiceImpl<UserAccount> implements UserAccountService {

    private final UserAccountDao userAccountDao;
    private final RoleDao roleDao;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public JpaRepository<UserAccount, Integer> getDao() {
        return userAccountDao;
    }

    @Override
    public UserAccount findByEmail(String email) {
        return userAccountDao.findByEmail(email);
    }

    @Override
    public UserAccount findByUsername(String username) {
        return userAccountDao.findByUsername(username);
    }

    public UserAccount registerNewUser(UserAccountDTO userAccountDTO) throws EmailExistsException {
        if (emailExists(userAccountDTO.getEmail())) {
            throw new EmailExistsException(
                    "There is an account with that email address: " + userAccountDTO.getEmail());
        }
        UserAccount userAccount = new UserAccount();
        userAccount.setUsername(userAccountDTO.getUsername());
        userAccount.setPassword(bCryptPasswordEncoder.encode(userAccountDTO.getPassword()));
        userAccount.setEmail(userAccountDTO.getEmail());
        userAccount.setEnabled(true);
        userAccount.setCreationDate(new Timestamp(new Date().getTime()));
        userAccount.setRoles(Arrays.asList(roleDao.findByName("ROLE_USER")));
        return super.save(userAccount);
    }

    public boolean emailExists(String email) {
        UserAccount userAccount = userAccountDao.findByEmail(email);
        return userAccount != null;
    }

    @Override
    public boolean usernameExists(String username) {
        UserAccount userAccount = userAccountDao.findByUsername(username);
        return userAccount != null;
    }

    public class EmailExistsException extends Exception {
        EmailExistsException(String message) {
            super(message);
        }
    }
}
