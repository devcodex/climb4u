package com.devcodex.climb4u.service.impl;

import com.devcodex.climb4u.dao.TopoReviewDao;
import com.devcodex.climb4u.model.Topo;
import com.devcodex.climb4u.model.TopoReview;
import com.devcodex.climb4u.model.UserAccount;
import com.devcodex.climb4u.service.TopoReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class TopoReviewServiceImpl extends GenericServiceImpl<TopoReview> implements TopoReviewService {
    @Autowired
    TopoReviewDao topoReviewDao;

    @Override
    public JpaRepository<TopoReview, Integer> getDao() {
        return topoReviewDao;
    }

    public ArrayList<TopoReview> findAllbyTopo(Topo topo) {
        return topoReviewDao.findAllByTopo(topo);
    }

    @Override
    public TopoReview findUserReview(Topo topo, UserAccount userAccount) {
        return topoReviewDao.findByTopoAndCreatedBy(topo, userAccount);
    }
}
