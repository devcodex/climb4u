package com.devcodex.climb4u.service.impl;

import com.devcodex.climb4u.dao.SiteDao;
import com.devcodex.climb4u.dao.TopoDao;
import com.devcodex.climb4u.model.Site;
import com.devcodex.climb4u.model.Topo;
import com.devcodex.climb4u.model.UserAccount;
import com.devcodex.climb4u.model.dto.SiteDTO;
import com.devcodex.climb4u.model.dto.TopoDTO;
import com.devcodex.climb4u.model.specification.SiteSpecification;
import com.devcodex.climb4u.model.specification.TopoSpecification;
import com.devcodex.climb4u.service.SiteService;
import com.devcodex.climb4u.service.TopoService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor(onConstructor = @_(@Autowired))
public class SiteServiceImpl extends GenericServiceImpl<Site> implements SiteService {
    private final SiteDao siteDao;

    @Override
    public JpaRepository<Site, Integer> getDao() {
        return siteDao;
    }

    @Override
    public Site findById(int id) throws NotFoundException {
        return siteDao.findById(id);
    }

    @Override
    public ArrayList<Site> findAllByAvailable(boolean available) {
        return siteDao.findAllByAvailableOrderByName(available);
    }

    @Override
    public ArrayList<Site> findAllByCreatedBy(UserAccount userAccount) {
        return siteDao.findAllByCreatedBy(userAccount);
    }

    @Override
    public ArrayList<Site> findAllByCriteria(SiteDTO siteDTO) {
        return (ArrayList<Site>) siteDao.findAll(
                SiteSpecification.siteDepartmentIdEquals(siteDTO.getDepartmentId())
                .and(SiteSpecification.siteIdEquals(siteDTO.getSiteId()))
                .and(SiteSpecification.siteTypeEquals(siteDTO.getType()))
                .and(SiteSpecification.siteHeightGreaterOrEqualTo(siteDTO.getHeight()))
                .and(SiteSpecification.siteDiffGreaterOrEqualTo(siteDTO.getDiffMin()))
                .and(SiteSpecification.siteDiffLessOrEqualTo(siteDTO.getDiffMax()))
                .and(SiteSpecification.siteIsOfficial(siteDTO.getOfficial())),
                new Sort(Sort.Direction.ASC, "name")
        );
    }

    @Override
    public Long countAll() {
        return siteDao.countAllBy();
    }

    @Override
    public Site addNewSite(Site site) {
        return siteDao.save(site);
    }
}
