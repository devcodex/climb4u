package com.devcodex.climb4u.service.impl;


import com.devcodex.climb4u.dao.AscentDao;
import com.devcodex.climb4u.dao.SectorDao;
import com.devcodex.climb4u.model.Ascent;
import com.devcodex.climb4u.model.Sector;
import com.devcodex.climb4u.service.AscentService;
import com.devcodex.climb4u.service.SectorService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AscentServiceImpl extends GenericServiceImpl<Ascent> implements AscentService {

    private final AscentDao ascentDao;

    @Override
    public JpaRepository<Ascent, Integer> getDao() {
        return ascentDao;
    }
}
