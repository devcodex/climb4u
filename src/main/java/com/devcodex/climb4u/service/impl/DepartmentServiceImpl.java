package com.devcodex.climb4u.service.impl;


import com.devcodex.climb4u.dao.DepartmentDao;
import com.devcodex.climb4u.dao.RoleDao;
import com.devcodex.climb4u.model.Department;
import com.devcodex.climb4u.model.Role;
import com.devcodex.climb4u.service.DepartmentService;
import com.devcodex.climb4u.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DepartmentServiceImpl extends GenericServiceImpl<Department> implements DepartmentService {

    private final DepartmentDao departmentDao;

    @Override
    public JpaRepository<Department, Integer> getDao() {
        return departmentDao;
    }
}
