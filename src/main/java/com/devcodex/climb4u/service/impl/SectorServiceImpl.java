package com.devcodex.climb4u.service.impl;


import com.devcodex.climb4u.dao.DepartmentDao;
import com.devcodex.climb4u.dao.SectorDao;
import com.devcodex.climb4u.model.Department;
import com.devcodex.climb4u.model.Sector;
import com.devcodex.climb4u.model.Site;
import com.devcodex.climb4u.service.DepartmentService;
import com.devcodex.climb4u.service.SectorService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SectorServiceImpl extends GenericServiceImpl<Sector> implements SectorService {

    private final SectorDao sectorDao;

    @Override
    public JpaRepository<Sector, Integer> getDao() {
        return sectorDao;
    }

    @Override
    public ArrayList<Sector> findAllBySite(Site site) {
        return sectorDao.findBySite(site);
    }
}
