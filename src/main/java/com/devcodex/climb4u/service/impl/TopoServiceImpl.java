package com.devcodex.climb4u.service.impl;

import com.devcodex.climb4u.dao.TopoDao;
import com.devcodex.climb4u.model.Topo;
import com.devcodex.climb4u.model.dto.TopoDTO;
import com.devcodex.climb4u.model.UserAccount;
import com.devcodex.climb4u.model.specification.TopoSpecification;
import com.devcodex.climb4u.service.TopoService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor(onConstructor = @_(@Autowired))
public class TopoServiceImpl extends GenericServiceImpl<Topo> implements TopoService {
    private final TopoDao topoDao;

    @Override
    public Topo findByTitle(String title) {
        return topoDao.findByTitle(title);
    }

    @Override
    public Topo findById(int id) throws NotFoundException {
        return topoDao.findById(id);
    }

    @Override
    public ArrayList<Topo> findAllByCriteria(TopoDTO topoDTO) {
        return (ArrayList<Topo>) topoDao.findAll(
                TopoSpecification.topoTitleContains(topoDTO.getTitle())
                .and(TopoSpecification.topoSite(topoDTO.getSite()))
                .and(TopoSpecification.topoAscentsBetween(topoDTO.getMinAscent(), topoDTO.getMaxAscent()))
                .and(TopoSpecification.topoMinDifficulty(topoDTO.getMinDifficulty()))
                .and(TopoSpecification.topoMaxDifficulty(topoDTO.getMaxDifficulty()))
                .and(TopoSpecification.topoHeightBetween(topoDTO.getMinHeight(), topoDTO.getMaxHeight()))
                .and(TopoSpecification.topoAvailable(topoDTO.isAvailable())),
                new Sort(Sort.Direction.ASC, "title")
        );
    }

    @Override
    public Long countAllByAvailable(boolean available) {
        return topoDao.countAllByAvailable(true);
    }

    @Override
    public ArrayList<Topo> findAllByAvailable(boolean available) {
        return topoDao.findAllByAvailableOrderByTitle(available);
    }

    @Override
    public ArrayList<Topo> findAllByCreatedBy(UserAccount userAccount) {
        return topoDao.findAllByCreatedBy(userAccount);
    }

    @Override
    public JpaRepository<Topo, Integer> getDao() {
        return topoDao;
    }

    public Topo addNewTopo(Topo topo) {
        return super.save(topo);
    }
}
