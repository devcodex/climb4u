package com.devcodex.climb4u.service.impl;


import com.devcodex.climb4u.dao.AscentDao;
import com.devcodex.climb4u.dao.SiteCommentDao;
import com.devcodex.climb4u.model.Ascent;
import com.devcodex.climb4u.model.Site;
import com.devcodex.climb4u.model.SiteComment;
import com.devcodex.climb4u.service.AscentService;
import com.devcodex.climb4u.service.SiteCommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SiteCommentServiceImpl extends GenericServiceImpl<SiteComment> implements SiteCommentService {

    private final SiteCommentDao siteCommentDao;

    @Override
    public JpaRepository<SiteComment, Integer> getDao() {
        return siteCommentDao;
    }

    public ArrayList<SiteComment> findAllBySite (Site site) {
        return siteCommentDao.findAllBySiteOrderByCreationDate(site);
    }
}
