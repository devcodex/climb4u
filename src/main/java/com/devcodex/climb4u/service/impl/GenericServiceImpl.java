package com.devcodex.climb4u.service.impl;

import java.util.List;
import java.util.Optional;

import com.devcodex.climb4u.service.GenericService;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;

@Slf4j
public abstract class GenericServiceImpl<T> implements GenericService<T> {

    public abstract JpaRepository<T, Integer> getDao();

    @Override
    public List<T> findAll() {
        return getDao().findAll();
    }

    @Override
    public T save(T entity) {
        return getDao().save(entity);
    }

    @Override
    public void delete(int id) {
        T entity = null;
        try {
            entity = find(id);
            getDao().delete(entity);
        } catch (NotFoundException e) {
            log.error("No entity found to delete with id", id);
        }
    }

    @Override
    public void update(T entity) {
        getDao().save(entity);
    }

    @Override
    public T find(int id) throws NotFoundException {
        Optional<T> entity = getDao().findById(id);
        if (entity.isPresent()) {
            return entity.get();
        } else {
            log.error("Can't find entity id {}", id);
            throw new NotFoundException("No entity with id " + id);
        }
    }

    @Override
    public T edit(T entity) {
        return getDao().saveAndFlush(entity);
    }
}

