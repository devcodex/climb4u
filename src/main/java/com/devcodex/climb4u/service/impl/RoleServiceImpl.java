package com.devcodex.climb4u.service.impl;


import com.devcodex.climb4u.dao.RoleDao;
import com.devcodex.climb4u.model.Role;
import com.devcodex.climb4u.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RoleServiceImpl extends GenericServiceImpl<Role> implements RoleService {

    private final RoleDao roleDao;

    @Override
    public JpaRepository<Role, Integer> getDao() {
        return roleDao;
    }

    @Override
    public Role findByName(String name) {
        return roleDao.findByName(name);
    }
}
