package com.devcodex.climb4u.service;

import com.devcodex.climb4u.model.Site;
import com.devcodex.climb4u.model.Topo;
import com.devcodex.climb4u.model.UserAccount;
import com.devcodex.climb4u.model.dto.SiteDTO;
import com.devcodex.climb4u.model.dto.TopoDTO;
import javassist.NotFoundException;

import java.util.ArrayList;

public interface SiteService extends GenericService<Site> {
    Site findById(int id) throws NotFoundException;
    ArrayList<Site> findAllByAvailable(boolean available);
    ArrayList<Site> findAllByCreatedBy(UserAccount userAccount);
    ArrayList<Site> findAllByCriteria(SiteDTO siteDTO);
    Long countAll();

    Site addNewSite(Site site);
}
