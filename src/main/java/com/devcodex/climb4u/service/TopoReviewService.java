package com.devcodex.climb4u.service;

import com.devcodex.climb4u.model.Topo;
import com.devcodex.climb4u.model.TopoReview;
import com.devcodex.climb4u.model.UserAccount;

import java.util.ArrayList;

public interface TopoReviewService extends GenericService<TopoReview> {
    ArrayList<TopoReview> findAllbyTopo(Topo topo);
    TopoReview findUserReview(Topo topo, UserAccount userAccount);
}
