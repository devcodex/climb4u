package com.devcodex.climb4u.service;

import com.devcodex.climb4u.model.Sector;
import com.devcodex.climb4u.model.Site;

import java.util.ArrayList;

public interface SectorService extends GenericService<Sector> {
    ArrayList<Sector> findAllBySite(Site site);
}
