package com.devcodex.climb4u.service;

import com.devcodex.climb4u.model.Topo;
import com.devcodex.climb4u.model.TopoLending;
import com.devcodex.climb4u.model.UserAccount;

import java.util.ArrayList;

public interface TopoLendingService extends GenericService<TopoLending> {
    ArrayList<TopoLending> findAllbyTopo(Topo topo);
    ArrayList<TopoLending> findAllByTakerAndTopo(UserAccount userAccount, Topo topo);
    ArrayList<TopoLending> findAllByTaker(UserAccount userAccount);
}
