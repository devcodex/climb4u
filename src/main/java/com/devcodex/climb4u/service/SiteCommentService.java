package com.devcodex.climb4u.service;

import com.devcodex.climb4u.model.Site;
import com.devcodex.climb4u.model.SiteComment;

import java.util.ArrayList;

public interface SiteCommentService extends GenericService<SiteComment> {
    ArrayList<SiteComment> findAllBySite(Site site);
}
