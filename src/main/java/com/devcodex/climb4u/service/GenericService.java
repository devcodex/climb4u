package com.devcodex.climb4u.service;

import java.util.List;
import javassist.NotFoundException;

public interface GenericService<T> {

    List<T> findAll();

    T save(T entity);

    void delete(int id);

    void update(T entity);

    T find(int id) throws NotFoundException;

    T edit(T entity);

}

