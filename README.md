# Climb4u - Climbing website

This web application serves to store and display data regarding climbing sites and topos.
In order to access full functionality users must be logged in, otherwise they can only view data

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

Java 8

A Java IDE

Maven - if you don't have it installed you can install
         it by clicking [here](https://maven.apache.org/download.cgi).
         
Activated Annotation Processing on your IDE, to see how to Activate Annotation Processing [click here](https://immutables.github.io/apt.html).

Git (if you wish to clone or fork the repository)

A PostgreSQL server running on your local machine (Recommended version 4 or higher)

### Installing

1. Start by retrieving the project from the current repository, to do this, simply clone the project,
or download the project to your local machine.
 
2. Open your IDE and start a new project from existing sources. Make sure to import the project as a Maven project.

3. On your PostGreSQL server create a database named climb4u 
(If you wish to do a manual initialisation of the database, use the script in file "manual_init" at the base of the project to initialise your databse, in order to do this, if you are using PgAdmin, simply right click your newly created database, select "Query Tool", and copy and run the script in this panel)

4. On the file climb4u\src\main\resources\application.properties, follow the instructions contained in this file
(Spring will take care of the rest, if you wish to initialise the database yourself, see section "Manual database initialisation" further down)
 
5. Once the project is imported, open a terminal/console on the root of the folder and execute the
following command:
````maven install````
This will download and install all necessary dependencies for the project to run.

6. This project uses Lombok to auto generate Getters and Setters 
(Lombok has many more uses that are not fully explored in this project),
and for it's proper functioning it is required that you have Annotation Processing activated in your IDE.

7. Run the automatically created run configuration.

The project should now be up and running.
Enjoy and thank you for using it.


## Built With
[Maven](https://maven.apache.org/) - Dependency Management

## Authors

**Bruno Ferreira**